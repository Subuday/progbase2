// Компілювати за допомогою:
// gcc main.c -lprogbase -lm

#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <stdbool.h>
#include <time.h>
#include <ctype.h> 
#include <string.h>
#include <assert.h>
#include <progbase.h> // Спрощений ввід і вивід даних у консоль

typedef struct List List;

struct List {
    int* items;
    size_t len;
    size_t capacity;
};

List* List_init(const int length);
void List_deinit(List* self);
void List_add(List* self, int num);
void List_sort(List* self);
const void List_print(List* self);
size_t List_size(const List* self);
char* List_get(const List* self, const size_t index); 
int List_indexOf(List* self, int key);
bool List_contains(List* self, int key);
void List_remove(List* self, int key);

void insertSort(int arr[], int n);
int binarySearch(int arr[], int left, int right, int key);


int main() {
    // Початок програми
    List* list = List_init(5);
    List_add(list, 5);
    List_add(list, 5);
    List_add(list, 5);
    List_add(list, 5);
    List_add(list, 5);
    List_add(list, 5);
    List_print(list);

    puts("");
    puts("");

    int index = List_indexOf(list, 4);
    if(index == -1) puts("BAD");
    else printf("%d", index);

    puts("");
    puts("");

    bool test = List_contains(list, 10);
    printf("%d", test);

    puts("");
    puts("");
    puts("");

    List_remove(list, 5);
    List_print(list);

    puts("");
    puts("");
    puts("");

    List_add(list, 84);
    List_add(list, 8);
    List_add(list, 34);
    List_add(list, 5);
    List_add(list, 73);
    List_add(list, 94); 
    List_add(list, 33);

    List_print(list);

    List_deinit(list);
    // Кінець програми
    return 0;
}




List* List_init(const int length) {
    if(length <= 0) {fprintf(stderr, "Memory can't be allocated ");  abort();}
    List* self = malloc(sizeof(struct List));
    if(self == NULL) {fprintf(stderr, "Memory can't be allocated ");  abort();}
    self->capacity = 4 * length;
    self->items = malloc(sizeof(int) * self->capacity);
    if(self->items == NULL) {fprintf(stderr, "Memory can't be allocated ");  abort();}
    self->len = 0;
    return self;
}

void List_deinit(List* self) {
    free(self->items);
    free(self);
}

void List_add(List* self, int num) {
    if(self->capacity == self->len) {
        size_t newCapacity = self->capacity * 4;
        int* newItems = realloc(self->items ,sizeof(int) * newCapacity); 
        if(newItems == NULL) {
            List_deinit(self);
            fprintf(stderr, "Memory can't be allocated ");
            abort();
        }
        self->capacity = newCapacity;
        self->items = newItems;
    }
    self->items[self->len] = num;
    self->len++;
    insertSort(self->items,  self->len);
    
}



const void List_print(List *self) {
    for(size_t i = 0; i < self->len; ++i) {
            printf("%d", self->items[i]);
            putchar('\n');
    }
}

size_t List_size(const List* self) {
    return self->len;
}

void insertSort(int arr[], int n) {
        int tmp = arr[n-1];
        int j = n - 2;
        while(j >= 0 && arr[j] > tmp) {
            arr[j+1] = arr[j];
            j--;
        }
        arr[j+1] = tmp;
}

int List_indexOf(List* self, int key) {
    int index = binarySearch(self->items, 0, self->len - 1, key);
}

int binarySearch(int arr[], int left, int right, int key) { 
    if (right >= left) { 
        int mid = left + (right - left) / 2; 
  
        if (arr[mid] == key) 
            return mid; 

        if (arr[mid] > key) 
            return binarySearch(arr, left, mid - 1, key); 

        return binarySearch(arr, mid + 1, right, key); 
    } 

    return -1; 
} 

bool List_contains(List* self, int key) {
    int index = binarySearch(self->items, 0, self->len - 1, key);
    if(index == -1) return false;

    return true;
}

void List_remove(List* self, int key) {
    int index = binarySearch(self->items, 0, self->len - 1, key);
    if(index == -1) return;

    for(size_t i = index; i < self->len - 1; ++i) {
        self->items[i] = self->items[i + 1];
    }
    self->len--;
}