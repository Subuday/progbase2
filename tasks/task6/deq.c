// Компілювати за допомогою:
// gcc main.c -lprogbase -lm

#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <stdbool.h>
#include <time.h>
#include <ctype.h> 
#include <string.h>
#include <assert.h>
#include <progbase.h> // Спрощений ввід і вивід даних у консоль

typedef struct Deque Deque;

struct Deque {
    int* items;
    int first;
    int last;
    int capacity;
};

Deque* Deque_alloc();
void Deque_free(Deque* self);
void Deque_pushBack(Deque* self, int num);
void Deque_pushFront(Deque* self, int num);
int Deque_popBack(Deque* self);
int Deque_popFront(Deque* self);
int Deque_size(Deque* self);
void Deque_print(Deque* self);

int main() {
    // Початок програми
    Deque* deque = Deque_alloc();
    Deque* deque2 = Deque_alloc();
    Deque* deque3 = Deque_alloc();

    for(int i = 0; i < 31; i+= 3) {
        Deque_pushBack(deque, i);
    }

    for(int i = 50; i >= 10; i -= 2) {
        Deque_pushBack(deque2, i);
    }

    Deque_print(deque);
    Deque_print(deque2);


    while(Deque_size(deque) != 0 || Deque_size(deque2) != 0) {
        int num = -1;
        if(Deque_size(deque) != 0) {
            num =  Deque_popFront(deque);
        }
        int num2 = -1;
        if(Deque_size(deque2) != 0) {
            num2 = Deque_popBack(deque2);
        }

        if(num >= num2) {
            Deque_pushBack(deque3, num2);
            Deque_pushFront(deque, num);
        } else {
            if(num == -1) {
                Deque_pushBack(deque3, num2);   
                continue;
            }
            Deque_pushBack(deque3, num);
            Deque_pushBack(deque2, num2);
        }
    }

    puts("");
    Deque_print(deque3);



    

    Deque_free(deque);
    Deque_free(deque2);
    Deque_free(deque3);


    // Кінець програми
    return 0;
}

Deque* Deque_alloc() {
    Deque* self = (Deque*)malloc(sizeof(struct Deque));
    if(self == NULL) abort();
    self->capacity  = 4;
    self->items = malloc(sizeof(int) * self->capacity);
    if(self->items == NULL) abort();
    self->first = 0;
    self->last = 0;
    return self;
}

void Deque_free(Deque* self) {
    free(self->items);
    free(self);
}

void Deque_pushBack(Deque* self, int num) {
    if(self->last == self->capacity) {
        int newCapacity = 4 * self->capacity;
        int* newItems = realloc(self->items, sizeof(int) * newCapacity);
        if(newItems == NULL) abort();
        self->capacity = newCapacity;
        self->items =newItems;
    }
    self->items[self->last] = num;
    self->last++;
}
void Deque_pushFront(Deque* self, int num) {
    if(self->first > 0) {self->first--; self->items[self->first] = num; return;}
    
    if(self->capacity == self->last) {
        size_t newCapacity = 4 * self->capacity;
        int* newItems = (int*)realloc(self->items, sizeof(int) * newCapacity);
        if(newItems == NULL) {fprintf(stderr, "Deque_pushBack: realloc newItems NULL"); abort();}
        self->items = newItems;
        self->capacity = newCapacity;
    }

    for(int i = self->last - 1; i >= 0; --i) {
        self->items[i + 1] = self->items[i];
    }
    self->items[self->first] = num;
    self->last++; 
}


int Deque_popBack(Deque* self) {
    self->last--;
    return self->items[self->last];
}
int Deque_popFront(Deque* self) {
    return self->items[self->first++];
}


int Deque_size(Deque* self) {
    return self->last - self->first;
}

void Deque_print(Deque* self) {
    for(size_t i = self->first; i < self->last; ++i) {
        printf("%d ", self->items[i]);
    }
}