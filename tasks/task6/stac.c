// Компілювати за допомогою:
// gcc main.c -lprogbase -lm

#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <stdbool.h>
#include <time.h>
#include <ctype.h> 
#include <string.h>
#include <assert.h>
#include <progbase.h> // Спрощений ввід і вивід даних у консоль

typedef struct Stack Stack;

struct Stack {
    char* items;
    int last;
    int capacity;
};


Stack* Stack_alloc();
void Stack_free(Stack* self);
void Stack_push(Stack* self, char c);

bool par(const char * str);

int main(void) {
    assert(par("<") == 0);
    assert(par("()") == 1);
    assert(par("><") == 0);
    assert(par("<(>)") == 0);
    assert(par("({}<>)") == 1);
    assert(par("([()](<>){})") == 1);
    assert(par("(){}<>") == 1);
    return 0;
}


Stack* Stack_alloc() {
    Stack* self = (Stack*)malloc(sizeof(struct Stack));
    if(self == NULL) abort();
    self->capacity  = 4;
    self->items = malloc(sizeof(char) * self->capacity);
    if(self->items == NULL) abort();
    self->last = 0;
    return self;
}

void Stack_free(Stack* self) {
    free(self->items);
    free(self);
}

void Stack_push(Stack* self, char c) {
    if(self->last == self->capacity) {
        int newCapacity = 4 * self->capacity;
        char* newItems = realloc(self->items, sizeof(char) * newCapacity);
        if(newItems == NULL) abort();
        self->capacity = newCapacity;
        self->items =newItems;
    }

    self->items[self->last] = c;
    self->last++;
}

char Stack_pop(Stack* self) {
    self->last--;
    return self->items[self->last];
}

char Stack_peek(Stack* self) {
    int last = self->last - 1;
    return self->items[last];
}

bool Stack_empty(Stack* self) {
    if(self->last == 0) return true;
    return false;
}
bool par(const char * str) {
    char c = '\0';
    Stack* stack = Stack_alloc();

    for(int i = 0; str[i] != '\0'; ++i) {
        c = str[i];
        switch(c) {
            case '<':       
            case '(': 
            case '[': 
            case '{': {
                Stack_push(stack, c);
                break;
            }
            
            case ')': {
                if(Stack_empty(stack) || Stack_peek(stack) != '(') {Stack_free(stack); return false;}
                Stack_pop(stack);
                break;
            }
            case ']': {
                if(Stack_empty(stack) || Stack_peek(stack) != '[') {Stack_free(stack); return false;}
                Stack_pop(stack);
                break;
            }
            case '}': {
                if(Stack_empty(stack)  || Stack_peek(stack) != '{') {Stack_free(stack); return false;}
                Stack_pop(stack);
                break;
            }
            case '>': {
                if(Stack_empty(stack) || Stack_peek(stack) != '<') {Stack_free(stack); return false;}
                Stack_pop(stack);
                break;
            }
        }
    }

    bool res =  Stack_empty(stack);
    Stack_free(stack);
    
    return res;
}



