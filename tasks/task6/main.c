// Компілювати за допомогою:
// gcc main.c -lprogbase -lm

#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <stdbool.h>
#include <time.h>
#include <ctype.h> 
#include <string.h>
#include <assert.h>
#include <progbase.h> // Спрощений ввід і вивід даних у консоль

int digitsSum(char* str, int i) {
    if(str[0] == '\0') {
        if(i > 5) return 1;
        return 0;
    }

    return digitsSum(str + 1, i + 1);
}
 

int main() {
    // Початок програми
    assert(digitsSum("", 0) == 0);
    assert(digitsSum("hello", 0) == 0);
    assert(digitsSum("hello1", 0) == 1);
    assert(digitsSum("hello102a3", 0) == 1);
    assert(digitsSum("2hello99", 0) == 1);
    assert(digitsSum("hello-1209hello", 0) == 1);

    




    // Кінець програми
    return 0;
}