// Компілювати за допомогою:
// gcc main.c -lprogbase -lm

#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <stdbool.h>
#include <time.h>
#include <ctype.h> 
#include <string.h>
#include <assert.h>
#include <progbase.h> // Спрощений ввід і вивід даних у консоль

typedef struct Set Set;

struct Set {
    int* items;
    int size;
    int capacity;
};

Set* Set_alloc();
void Set_free(Set* self);
size_t Set_size(Set* self);
void   Set_insert   (Set * self, int value);  // add [unique] value 
bool   Set_contains (Set * self, int value);  // check for value 
void   Set_remove   (Set * self, int value);  // remove value 

Set *  Set_createUnion               (Set * self, Set * other);
Set *  Set_createIntersection        (Set * self, Set * other);
Set *  Set_createDifference          (Set * self, Set * other);
Set *  Set_createSymmetricDifference (Set * self, Set * other);
bool   Set_isSubset                  (Set * self, Set * other);

void Set_print(Set * self);

int main() {
    // Початок програми
    Set* set1 = Set_alloc();
    Set* set2 = Set_alloc();

    for(int i = 0; i < 10; ++i) {
        Set_insert(set1, i);
    }
    
    for(int i = 10; i < 20; ++i) {
        Set_insert(set2, i);
    }

    Set* set3 = Set_createUnion(set1, set2);
    Set* set4 = Set_createIntersection(set1, set3);
    Set* set5 = Set_createDifference(set3, set1);

    Set_print(set3);
    Set_print(set4);
    Set_print(set5);

    Set* set6 = Set_alloc();
    Set_insert(set6, 3);
    Set_insert(set6, 4);
    Set_insert(set6, 5);

    Set* set7 = Set_alloc();
    Set_insert(set7, 5);
    Set_insert(set7, 8);
    Set_insert(set7, 9);

    Set* set8 = Set_createSymmetricDifference(set7, set6);

    Set_print(set8);

    Set_free(set1);
    Set_free(set2);
    Set_free(set3);
    Set_free(set4);
    Set_free(set5);
    Set_free(set6);
    Set_free(set7);
    Set_free(set8);
    

    // Кінець програми

    return 0;
}

Set* Set_alloc() {
    Set* self = (Set*)malloc(sizeof(struct Set));
    if(self == NULL) abort();
    self->capacity  = 4;
    self->items = malloc(sizeof(int) * self->capacity);
    if(self->items == NULL) abort();
    self->size = 0;
    return self;
}

void Set_free(Set* self) {
    free(self->items);
    free(self);
} 

size_t Set_size(Set* self) {
    return self->size;
}

void   Set_insert   (Set * self, int value) {
    if(self->size == self->capacity) {
        int newCapacity = 4 * self->capacity;
        int* newItems = realloc(self->items, sizeof(int) * newCapacity);
        if(newItems == NULL) abort();
        self->capacity = newCapacity;
        self->items =newItems;
    }
    self->items[self->size] = value;
    self->size++;
}

bool   Set_contains (Set * self, int value) {
    for(int i = 0; i < self->size; ++i) {
        if(self->items[i] == value) return true;
    }
    return false;
}

void   Set_remove   (Set * self, int value) {
    for(int i = 0; i < self->size; ++i) {
        if(self->items[i] == value) {
            for(int j = i; j < self->size; ++j) {
                self->items[j] = self->items[j+1];
            }
            return;
        }
    }
}

Set *  Set_createUnion (Set * self, Set * other) {
    Set* newSet = Set_alloc();
    
    for(int i = 0; i < self->size; ++i) {
        int num = self->items[i];
        Set_insert(newSet, num);
    }
    
    for(int i = 0; i < other->size; ++i) {
        int num = other->items[i];
        Set_insert(newSet, num);
    }
    
    return newSet;
}

void Set_print(Set * self) {
    for(int i = 0; i < self->size; ++i) {
        printf("%d ", self->items[i]);
    }
    puts(" ");
}

Set *  Set_createIntersection (Set * self, Set * other) {
    Set* newSet = Set_alloc();
    
    if(self->size >= other->size) {
        for(int i = 0; i < other->size; ++i) {
            int num = other->items[i];
            if(Set_contains(self, num)) {
                Set_insert(newSet, num);
            }
        }
    } else {
        for(int i = 0; i < self->size; ++i) {
            int num = self->items[i];
            if(Set_contains(other, num)) {
                Set_insert(newSet, num);
            }
        }
    }

    return newSet;
}

Set *  Set_createDifference  (Set * self, Set * other) {
    Set* newSet = Set_alloc();

    for(int i = 0; i < self->size; ++i) {
        int num = self->items[i];
        if(Set_contains(other, num)) continue;

        Set_insert(newSet, num);
    }

    return newSet;
}

Set *  Set_createSymmetricDifference (Set * self, Set * other) {
    Set* newSet = Set_alloc();
    
    for(int i = 0; i < self->size; ++i) {
        int num = self->items[i];
        if(Set_contains(other, num)) continue;
        Set_insert(newSet, num);
    }

    for(int i = 0; i <  other->size; ++i) {
        int num = other->items[i];
        if(Set_contains(self, num)) continue;
        Set_insert(newSet, num);
    }

    return newSet;
}
