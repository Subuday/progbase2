// Компілювати за допомогою:
// gcc main.c -lprogbase -lm

#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <stdbool.h>
#include <time.h>
#include <ctype.h> 
#include <string.h>
#include <assert.h>
#include <progbase.h> // Спрощений ввід і вивід даних у консоль

int main() {
    // Початок програми
    int a = 15;
    int *x = NULL;

    x = malloc(sizeof(int));
    *x = 14;
    free(x);

    x = &a;
    x = malloc(sizeof(int));
    free(x);

    x = NULL;

    char* pstr = malloc(sizeof(char) * 64);
    strcpy(pstr, "Hello World");
    int **par = malloc(sizeof(int *) * 2);

    puts(pstr);
    free(pstr);

    *par = NULL;
    *(par + 1) = NULL;
    free(par);

    int *par2 = malloc(sizeof(int) * 2);

    *par2 = 2;
    *(par2 + 1)= 3;

    free(par2);

    pstr = "READONlY";

    puts(pstr);

    // Кінець програми
    return 0;
}