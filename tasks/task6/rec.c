// Компілювати за допомогою:
// gcc main.c -lprogbase -lm

#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <stdbool.h>
#include <time.h>
#include <ctype.h> 
#include <string.h>
#include <assert.h>
#include <progbase.h> // Спрощений ввід і вивід даних у консоль

int charSum(const char * const str);

int main() {
    // Початок програми
    // assert(charSum("") == 0);
    // assert(charSum("hello") == 0);
    // assert(charSum("hello1") == 1);
    // assert(charSum("hello102a3") == 1);
    // assert(charSum("2hello99") == 1);
    // assert(charSum("hello-1209hello") == 1);



    char* str = "a";

    int sum = charSum(str);
    printf("\nTask: get sum of all digits in string: %i\n\n", sum);

    // Кінець програми
    return 0;
}

int charSum(const char * const str) {
    const char firstChar = str[0];

    if(*str == '\0') return 0;
   
    int sum = 1 + charSum(str + 1);

    if(sum > 5 || sum == 0) return 0;

    return 0;


    
}