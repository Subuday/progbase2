// Компілювати за допомогою:
// gcc main.c -lprogbase -lm

#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <stdbool.h>
#include <time.h>
#include <ctype.h> 
#include <string.h>
#include <assert.h>
#include <progbase.h> // Спрощений ввід і вивід даних у консоль

typedef struct __IntBinTree IntBinTree;
struct __IntBinTree {
    int key;
    IntBinTree * left;
    IntBinTree * right;
};

typedef struct __IntBSTree IntBSTree;
struct __IntBSTree {
    IntBinTree * root;  
};

IntBSTree * IntBSTree_new(void);
void IntBSTree_free(IntBSTree * self);
void IntBSTree_insert(IntBSTree * self, int key);

IntBinTree * IntBinTree_new(int key);
void IntBinTree_free(IntBinTree * self);
IntBinTree* IntBinTree_insert(IntBinTree * self, int key);


void printPreorder(IntBinTree * self);
void printPostorder(IntBinTree * self);

int main() {
    // Початок програми
    IntBSTree* tree =  IntBSTree_new();
    IntBSTree_insert(tree, 1);
    IntBSTree_insert(tree, -13);
    IntBSTree_insert(tree, 12);
    IntBSTree_insert(tree, 0);  
    IntBSTree_insert(tree, 6);
    IntBSTree_insert(tree, 5);

    printPreorder(tree->root);
    puts(" ");
    printPostorder(tree->root);

    IntBSTree_free(tree);
    // Кінець програми
    return 0;
}

IntBSTree * IntBSTree_new(void) {
    IntBSTree* self = malloc(sizeof(struct __IntBSTree));
     if(self == NULL) {fprintf(stderr, "IntBSTree_new malloc failed");}
     self->root = NULL;
     return self;
}

void IntBSTree_free(IntBSTree * self) {
    IntBinTree_free(self->root);
    free(self);
}

void IntBSTree_insert(IntBSTree * self, int key) {
    self->root = IntBinTree_insert(self->root, key);
}

IntBinTree * IntBinTree_new(int key) {
    IntBinTree* self = malloc(sizeof(struct __IntBinTree));
    if(self == NULL) {fprintf(stderr, "IntBinTree_new malloc failed");}
    self->right = NULL;
    self->left = NULL;
    self->key = key;
    return self;
}

void IntBinTree_free(IntBinTree * self) {
    if(self == NULL) return; 

    IntBinTree_free(self->left);
    IntBinTree_free(self->right);

    free(self);
}

IntBinTree* IntBinTree_insert(IntBinTree * self, int key) {
    if (self == NULL) return IntBinTree_new(key);

    if (key < self->key) 
        self->left  = IntBinTree_insert(self->left, key); 
    else if (key > self->key) 
        self->right = IntBinTree_insert(self->right, key); 

    return self;
}

void printPostorder(IntBinTree * self) { 
     if (self == NULL) 
        return; 

     printPostorder(self->left); 
     printPostorder(self->right); 
  
     printf("%d ", self->key); 
} 

void printPreorder(IntBinTree * self) { 
     if (self == NULL) 
          return; 
  
     printf("%d ", self->key);   
  
     printPreorder(self->left);   
     printPreorder(self->right); 
} 
