#include "bintree.h"

IntBinTree * IntBinTree_new(int key) {
    IntBinTree* self = malloc(sizeof(struct __IntBinTree));
    if(self == NULL) {fprintf(stderr, "IntBinTree_new malloc failed");}
    self->right = NULL;
    self->left = NULL;
    self->key = key;
    return self;
}

void IntBinTree_free(IntBinTree * self) {
    if(self == NULL) return; 

    IntBinTree_free(self->left);
    IntBinTree_free(self->right);

    free(self);
}

IntBinTree* IntBinTree_insert(IntBinTree * self, int key) {
    if (self == NULL) return IntBinTree_new(key);

    if (key < self->key) 
        self->left  = IntBinTree_insert(self->left, key); 
    else if (key > self->key) 
        self->right = IntBinTree_insert(self->right, key); 

    return self;
}

bool IntBinTree_lookup(IntBinTree * self, int key) {
    if(self == NULL) return false;

    if(self->key == key) return true;

    bool leftBranch = IntBinTree_lookup(self->left, key);
    bool rightBranch = IntBinTree_lookup(self->right, key);

    return leftBranch || rightBranch;
}

IntBinTree* IntBinTree_delete(IntBinTree * self, int key) {
    if(self == NULL) return self;

    if(key > self->key) self->right = IntBinTree_delete(self->right, key);
    else if(key < self->key) self->left = IntBinTree_delete(self->left, key);
    else {
        if(self->left == NULL) {
            IntBinTree* temp = self->right;
            free(self);
            return temp;
        } else if(self->right == NULL) {
            IntBinTree* temp = self->left;
            free(self);
            return temp;
        } else {
            IntBinTree* temp = self->right;
            IntBinTree* prntTemp = self->right;

            while(temp->left != NULL) {
                prntTemp = temp;
                temp = temp->left;
            }

            prntTemp->left = temp->right;
            self->key = temp->key;
            free(temp);
            return self;
        }

    }
    
    return self;
}

void IntBinTree_printFormat(IntBinTree * self) {
    if(self == NULL) return;

    IntBinTree_printFormat(self->left);

    printf("%d, ", self->key);

    IntBinTree_printFormat(self->right);
}

void traverseInOrder(IntBinTree *node)
{
   if (node == NULL) return;

   traverseInOrder(node->left);   // left
   printf("%i, ",  node->key);  // current
   traverseInOrder(node->right);  // right
}

static void printValueOnLevel(IntBinTree * node, char pos, int lvl)
{
   for (int i = 0; i < lvl; i++) {
       printf("....");
   }
   printf("%c: ", pos);

   if (node == NULL) {
       printf("(null)\n");
   } else {
       printf("%i\n", node->key);
   }
}

static void printNode(IntBinTree * node, char pos, int lvl)
{
   printValueOnLevel(node, pos, lvl);
   bool hasChild = node != NULL && (node->left != NULL || node->right != NULL);
   if (hasChild) {
       printNode(node->left,  'L', lvl + 1);
       printNode(node->right, 'R', lvl + 1);
   }
}

void printBinTree(IntBinTree * root) { printNode(root, '+', 0); }




