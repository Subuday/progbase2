// Компілювати за допомогою:
// gcc main.c -lprogbase -lm

#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <stdbool.h>
#include <time.h>
#include <ctype.h> 
#include <string.h>
#include <assert.h>
#include <progbase.h> // Спрощений ввід і вивід даних у консоль
 
#include "bintree.h"
#include "bstree.h"

int main() {
    // Початок програми
    IntBSTree* tree =  IntBSTree_new();
    IntBSTree_insert(tree, -13);
    IntBSTree_insert(tree, 12);
    IntBSTree_insert(tree, 0);
    IntBSTree_insert(tree, 1);
    IntBSTree_insert(tree, 6);
    IntBSTree_insert(tree, 5);

    bool res = IntBSTree_lookup(tree, 5);
    assert(res == true);

    IntBSTree_delete(tree, -13);
    IntBSTree_printFormat(tree);
    IntBSTree_printTraverse(tree);

    IntBSTree_free(tree);
    // Кінець програми
    return 0;
}