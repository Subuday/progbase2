#pragma once

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "bintree.h"

typedef struct __IntBSTree IntBSTree;
struct __IntBSTree {
    IntBinTree * root;  
};

IntBSTree * IntBSTree_new(void);
void IntBSTree_free(IntBSTree * self);

void IntBSTree_insert(IntBSTree * self, int key);
bool IntBSTree_lookup(IntBSTree * self, int key);
void IntBSTree_delete(IntBSTree * self, int key);

/**
 *  @brief remove all tree nodes and free their memory
 *  BST becomes empty and can be used to fill again
 */
void IntBSTree_clear(IntBSTree * self);

// extra

void IntBSTree_printFormat(IntBSTree * self);
void IntBSTree_printTraverse(IntBSTree * self); 