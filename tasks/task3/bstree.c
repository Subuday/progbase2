#include "bstree.h"

IntBSTree * IntBSTree_new(void) {
    IntBSTree* self = malloc(sizeof(struct __IntBSTree));
     if(self == NULL) {fprintf(stderr, "IntBSTree_new malloc failed");}
     self->root = NULL;
     return self;
}

void IntBSTree_free(IntBSTree * self) {
    IntBinTree_free(self->root);
    free(self);
}

void IntBSTree_insert(IntBSTree * self, int key) {
    self->root = IntBinTree_insert(self->root, key);
}

bool IntBSTree_lookup(IntBSTree * self, int key) {
    return IntBinTree_lookup(self->root, key);  
}

void IntBSTree_delete(IntBSTree * self, int key) {
    self->root = IntBinTree_delete(self->root, key);
}

void IntBSTree_clear(IntBSTree * self) {
    IntBinTree_free(self->root);
}


void IntBSTree_printFormat(IntBSTree * self) {
    IntBinTree_printFormat(self->root);
}

void IntBSTree_printTraverse(IntBSTree * self) {
    printBinTree(self->root);
}