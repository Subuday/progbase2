#pragma once

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

typedef struct __IntBinTree IntBinTree;
struct __IntBinTree {
    int key;
    IntBinTree * left;
    IntBinTree * right;
};

IntBinTree * IntBinTree_new(int key);
void IntBinTree_free(IntBinTree * self);
IntBinTree* IntBinTree_insert(IntBinTree * self, int key);
bool IntBinTree_lookup(IntBinTree * self, int key);
IntBinTree* IntBinTree_delete(IntBinTree * self, int key);
void IntBinTree_printFormat(IntBinTree * self);
void printBinTree(IntBinTree * root);