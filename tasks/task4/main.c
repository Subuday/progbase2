// Компілювати за допомогою:
// gcc main.c -lprogbase -lm

#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <stdbool.h>
#include <time.h>
#include <ctype.h> 
#include <string.h>
#include <assert.h>
#include <progbase.h> // Спрощений ввід і вивід даних у консоль

// List module
typedef struct __List List;
struct __List
{
    void *items[100];
    int size;
};

void List_add(List *self, void *value);
void *List_get(List *self, int index);
int List_size(List *self);

//
void List_add(List *self, void *value) { self->items[self->size++] = value; }
void *List_get(List *self, int index) { return self->items[index]; }
int List_size(List *self) { return self->size; }

// Tree module
typedef struct __Tree Tree;
struct __Tree
{
    void *key;
    void *value;
    List children;
};

Tree *buildContentsTree(int size, Header headers[]);
void printContentsTree(Tree *root);
void cleanupContentsTree(Tree *root);

Tree *Tree_alloc(void *key, void *val) { return memcpy(malloc(sizeof(Tree)), &((Tree){key, val}), sizeof(Tree)); }
void Tree_free(Tree *self) { free(self); }
//

typedef struct __Header Header;
struct __Header
{
    int id;
    char title[1000];
    int parentId;
};

Tree *buildContentsTree(int size, Header headers[]);
void printContentsTree(Tree *root);
void cleanupContentsTree(Tree *root);

int main()
{
    // Header headers[] = {
    //     {0, "Contents", -1},
    //     {1, "Overview", 0},
    //     {2, "Relations to other languages", 1},
    //     {3, "History", 0},
    //     {4, "Early developments", 3},
    //     {5, "K&R C", 3},
    //     {6, "ANSI C and ISO C", 3},
    //     {7, "Syntax", 0},
    //     {8, "Character set", 7},
    //     {9, "Reserved words", 7},
    //     {10, "Operators", 7},
    // };
    // const int size = sizeof(headers) / sizeof(headers[0]);
    // Tree *tree = buildContentsTree(size, headers);
    // printContentsTree(tree);
    // cleanupContentsTree(tree);
    return 0;
}

Header *findHeaderById(int size, Header headers[], int id);


static Tree *createHeaderTreeNode(Header *header, int size, Header headers[])
{
    Tree *node = Tree_alloc(&header->id, header);
    for (int i = 0; i < size; i++)
    {
        Header *h = &headers[i];
        if (h->parentId == header->id)
        {
            Tree *childNode = createHeaderTreeNode(h, size, headers);
            List_add(&node->children, childNode);
        }
    }
    return node;
}

Tree *buildContentsTree(int size, Header headers[])
{
    Header *rootHeader = findHeaderById(size, headers, 0);
    Tree *rootTreeNode = createHeaderTreeNode(rootHeader, size, headers);
    return rootTreeNode;
}

Header *findHeaderById(int size, Header headers[], int id)
{
    for (int i = 0; i < size; i++)
    {
        if (headers[i].id == id)
            return &headers[i];
    }
    return NULL;
}

Tree *buildContentsTree(int size, Header headers[])
{
    Header *rootHeader = findHeaderById(size, headers, 0);
    Tree *rootTreeNode = createHeaderTreeNode(rootHeader, size, headers);
    return rootTreeNode;
}

static void cleanupTreeNode(Tree *node)
{
    for (int i = 0; i < List_size(&node->children); i++)
    {
        Tree *child = List_get(&node->children, i);
        cleanupTreeNode(child);
    }
    Tree_free(node);
}

void cleanupContentsTree(Tree *root)
{
    cleanupTreeNode(root);
}



static void printContentsTreeLvl(Tree *node, int lvl);

void printContentsTree(Tree *root)
{
    printContentsTreeLvl(root, 0);
}

static void printHeaderNode(Tree *node)
{
    Header *header = node->value;
    printf("[%s]\n", header->title);
}

static void printContentsTreeLvl(Tree *node, int lvl)
{
    for (int i = 0; i < lvl; i++)
    {
        printf("..");
    }
    if (node == NULL)
    {
        printf("(NULL)");
    }
    else
    {
        printHeaderNode(node);
        for (int i = 0; i < List_size(&node->children); i++)
        {
            Tree *child = List_get(&node->children, i);
            printContentsTreeLvl(child, lvl + 1);
        }
    }
}