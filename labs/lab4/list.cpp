#include "list.hpp"

List::List() :  _size(0), _capacity(4) {
    this->_items = new string* [this->_capacity];
    if(this->_items == nullptr) abort();    
    
}

List::~List() {
    for(size_t i = 0; i < this->_size; ++i) {
        if(this->_items[i] != nullptr) delete this->_items[i];
    }
    delete [] this->_items;
}

void List::resize() {
    
    string** _items = new string* [this->_capacity * 4];
    if(_items == nullptr) abort();
    for(size_t i = 0; i < this->_size; ++i) {
        _items[i] = this->_items[i];
    }
    delete [] this->_items;
    
    this->_capacity = this->_capacity * 4;
    this->_items = _items;
}

void List::add(const string & str) {
    if(_capacity == _size) {
        resize();
    }

    string* newStr = new string(str);
    if(newStr == nullptr) abort();
    _items[_size] = newStr;
    _size++;
}

void List::sort() {
    for(size_t i = 0; i < _size; ++i) {
        if(_items[i]->length() < 10) {
            string* temp = _items[i];
            for(size_t j = i; j >= 1; --j) {
                _items[j] = _items[j-1];
            }
            _items[0] = temp;
        }
    }
}

void List::print() const {
    for(size_t i = 0; i < _size; ++i) {
        cout << *this->_items[i] << endl;
    }
}

size_t List::size() const {
    return this->_size;
}

string & List::operator [](int index) {
    return *(this->_items[index]);
}
