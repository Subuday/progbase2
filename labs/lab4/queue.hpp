#pragma once

#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;

class Queue 
{
public:

    Queue();
    ~Queue();

    void push(const string & str);
    string pop();
    size_t size() const;
    bool isEmpty() const;
    void print() const;



private:
    string** _items;
    size_t _capacity;
    size_t _first;
    size_t _last;

    void resize();
};

