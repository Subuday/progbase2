// Компілювати за допомогою:
// gcc main.c -lprogbase -lm
#include <iostream>    // Для друку в термінал
#include <cstdlib>   // Деякі додаткові функції
#include <fstream>
#include <string>
#include "list.hpp"
#include "queue.hpp"

void ListToQueue(List & list, Queue & q, Queue & q2);
void QueuesToList(List & list, Queue & q, Queue & q2);

int main() {

    ifstream file;
    file.exceptions (ifstream::failbit | ifstream::badbit);

    List list;
    Queue q;
    Queue q2;

    try {
        file.open("../data.txt");
        string line;
        while(!file.eof()) {
            getline(file, line);
            list.add(line);
        } 
        file.close();
    } catch(const ios_base::failure & e) {
        file.close();
        cerr << "Execption  opening/reading/closing file" << flush;
        cerr << e.what() << flush;
    }

    list.print();
    list.sort();   
    cout << "----------------------TASK 1-------------------------------------------" << endl;
    list.print();

    cout << "-----------------------TASK 2---------------------------------------------" << endl;
    ListToQueue(list, q, q2);
    q.print();
    cout << "/////////////////////////////////////////////////////////////////////////" << endl;
    q2.print();

    cout << "------------------------TASK 3-------------------------------------------" << endl;
    List list2; 
    QueuesToList(list2, q, q2);
    list2.print(); 
    
    // Кінець програми 
    return 0;
}

void ListToQueue(List & list, Queue & q, Queue & q2) {
    for(size_t i = 0; i < list.size(); ++i) {
        if(i % 2 == 0) q2.push(list[i]); 
        else q.push(list[i]);  
    }
}

void QueuesToList(List & list, Queue & q, Queue & q2) {
    while(!q.isEmpty()) {
        list.add(q.pop());
    }

    while(!q2.isEmpty()) {
        list.add(q2.pop());
    }
}