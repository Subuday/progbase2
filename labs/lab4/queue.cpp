#include "queue.hpp"

Queue::Queue() : _capacity(4), _first(0), _last(0) {
    this->_items = new string* [this->_capacity];
    if(this->_items == nullptr) abort();
}

Queue::~Queue() {
    for(size_t i = this->_first; i < this->_last; ++i) {
        if(this->_items[i] != nullptr) delete this->_items[i];
    }
    delete [] this->_items;
}

void Queue::push(const string & str) {
    if(_last == _capacity) {
        resize();
    }

    string* newStr = new string(str);
    if(newStr == nullptr) abort();
    _items[_last] = newStr;
    _last++;
}

string Queue::pop() {
    if(_items[_first] != nullptr) {
        string str = *_items[_first];
        delete _items[_first++];
        return str;
    }
    return "";
} 

void Queue::resize() {
    if(_first > 0) {
        for(size_t i = 0; i < _first; ++i) {
            for(size_t j = 0; j < _last -1; ++j) {
                _items[j] = _items[j+1];
            }
        }
        _last = _last - _first;
        _first = 0;
        return;
    } else {
        string** _items = new string* [this->_capacity * 4];
        if(_items == nullptr) abort();
        for(size_t i = 0; i < this->_last; ++i) {
            _items[i] = this->_items[i];
        }
        delete [] this->_items;
    
        this->_capacity = this->_capacity * 4;
        this->_items = _items;
    }
}

size_t Queue::size() const {
    return this->_last - this->_first;
}

bool Queue::isEmpty() const {
    return size() == 0 ? true : false;
}

void Queue::print() const {
    for(size_t i = _first; i < _last; ++i) {
        cout  << *_items[i] << endl;
    }
}

