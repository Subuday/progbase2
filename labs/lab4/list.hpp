#pragma once

#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;

class List 
{
public:

    List();
    ~List();

    string & operator [](int index);

    void add(const string & str);
    void print() const;
    void sort();
    size_t size() const;
    

private:
    string** _items;
    size_t _size;
    size_t _capacity;

    void resize();
};
