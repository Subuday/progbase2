#pragma once

#include <stdlib.h>
#include <stdio.h> 
#include <string.h>

typedef struct List List;

struct List {
    void** items;
    size_t size;
    size_t capacity;
};

void List_init(List* self);
void List_deinit(List* self);
void List_add(List* list, void* item);