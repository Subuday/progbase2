#include "list.h"

void List_init(List* self) {
    self->capacity = 4;
    self->items = malloc(sizeof(void*) * self->capacity);
    if(self->items == NULL) {fprintf(stderr, "MEMORY ERROR"), abort();}
    self->size = 0;
}

void List_deinit(List* self) {
    free(self->items);
} 

void List_add(List* list, void* item) {
    if(list->size == list->capacity) {
        int newCapacity = 4 * list->capacity;
        void* newItems = realloc(list->items, sizeof(void*) * newCapacity);
        if(newItems == NULL) {fprintf(stderr, "fillStudentsList: realloc error"); abort();}
        list->capacity = newCapacity;
        list->items = newItems;
    }
    list->items[list->size] = item;
    list->size++;
}