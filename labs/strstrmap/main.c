// Компілювати за допомогою:
// gcc *.c -lprogbase -lm

#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <stdbool.h>
#include <time.h>
#include <ctype.h> 
#include <string.h>
#include <assert.h>
#include <progbase.h> // Спрощений ввід і вивід даних у консоль

#include "list.h"
#include "map.h"
#include "str.h"

typedef struct __Student Student;
struct __Student
{
    int id;
    char fullname[100];
    double score;
};

void fillStudentsList(List * list, int studentsArrLen, Student studentsArr[]);
void printStudentMaps(List * list);

StrStrMap * createStudentMap(Student * student);
void cleanupMaps(List* list);

void addBonusScores(List* list, double score);
void addGroupNameValue(List* list, char* name);
void removeScoreValue(List* list);

int main() {
    // Початок програми
    Student studentsArr[] = {
        {0, "Student 1", 10.5},
        {1, "Student 2", 14.5},
        {2, "Student 3", 45.45},
        {4, "Student 4", 100.0},
        {5, "Student 5", 53.0},
        {6, "Student 6", 50.0},
        {7, "Student 7", 18.0},
        {8, "Student 8", 1.0},

    };
    const int studentsArrLen = sizeof(studentsArr) / sizeof(studentsArr[0]);

    List students;
    List_init(&students);

    fillStudentsList(&students, studentsArrLen, studentsArr);

    addBonusScores(&students, 10.0);
    printStudentMaps(&students);
    puts("");
    addGroupNameValue(&students, "KP-90");
    printStudentMaps(&students);
    puts("");
    removeScoreValue(&students);
    printStudentMaps(&students);

    cleanupMaps(&students);
    List_deinit(&students);


    return 0;
    // Кінець програми
    return 0;
}

void fillStudentsList(List * list, int studentsArrLen, Student studentsArr[]) {

    for (int i = 0; i < studentsArrLen; i++) {
        Student * student = &studentsArr[i];
        StrStrMap * studentMap = createStudentMap(student);
        List_add(list, studentMap);
    }
}

StrStrMap * createStudentMap(Student * student) {
    char * idValue = String_allocFromInt(student->id);
    char * fullnameValue = String_allocCopy(student->fullname);
    char * scoreValue = String_allocFromDouble(student->score);
    //
    StrStrMap * map = StrStrMap_alloc();
    StrStrMap_add(map, "id", idValue);
    StrStrMap_add(map, "fullname", fullnameValue);
    StrStrMap_add(map, "score", scoreValue);
    //
    assert(StrStrMap_contains(map, "id"));
    assert(StrStrMap_contains(map, "fullname"));
    assert(StrStrMap_contains(map, "score"));
    //
    return map;
}

void cleanupMaps(List* list) {
    for(int i = 0; i < list->size; ++i) {
        StrStrMap_clear((StrStrMap *)list->items[i]);
        Map_free((StrStrMap *)list->items[i]);
    }
}

void printStudentMaps(List * list) {
    for(int i = 0; i < list->size; ++i) {
        StrStrMap * map = list->items[i];
        for(int j = 0; j < map->size; ++j) {
            printf("%s: %s |", (char*)map->keys[j], (char*)map->values[j]);
        }
        putchar('\n');
    }
}

void addBonusScores(List* list, double score) {
    for(int i = 0; i < list->size; ++i) {
        StrStrMap * map = list->items[i];
        char* contain = (char*)StrStrMap_get(map, "score");
        if(contain == NULL) return;
        double value = atof(contain);
        value += score;
        sprintf(contain, "%f", value);
    }
}

void addGroupNameValue(List* list, char* name) {
    for(int i = 0; i < list->size; ++i) {
        StrStrMap * map = list->items[i];
        char* value = String_allocCopy(name);
        StrStrMap_add(map, "groupName", value);
    }
}

void removeScoreValue(List* list) {
    for(int i = 0; i < list->size; ++i) {
        StrStrMap * map = list->items[i];
        void* removeVal = Map_remove(map, "score");
        free(removeVal);
    }
}