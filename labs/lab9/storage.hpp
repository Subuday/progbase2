#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>

#include "optional.hpp"
#include "writer.hpp"
#include "book.hpp"
#include "user.h"

using namespace std;

class Storage
{
 public:
   virtual bool load() = 0;
   virtual bool save() = 0;
   // writers
   virtual vector<Writer> getAllWriters(void) = 0;
   virtual vector<Writer> getAllUserWriters(int user_id) = 0;
   virtual optional<Writer> getWriterById(int writer_id) = 0;
   virtual bool updateWriter(const Writer &writer) = 0;
   virtual bool removeWriter(const Writer &writer) = 0;
   virtual int insertWriter(const Writer &writer) = 0;
   // books
   virtual vector<Book> getAllBooks(void) = 0;
   virtual optional<Book> getBookById(int book_id) = 0;
   virtual bool updateBook(const Book &book) = 0;
   virtual bool removeBook(int book_id) = 0;
   virtual int insertBook(const Book &book) = 0;
   //users
   virtual optional<User> getUserAuth(string & username, string & passhash) = 0;
   //links
   virtual vector<Book> getAllWriterBooks(int writer_id) = 0;
   virtual bool insertWriterBook(int writer_id, int book_id) = 0;
   virtual bool removeWriterBook(int writer_id, int book_id) = 0;

   virtual ~Storage() { }
};

