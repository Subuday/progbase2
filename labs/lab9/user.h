#ifndef USER_H
#define USER_H

#include <QString>

class User
{
    int id;
    QString username;
    QString passhash;

public:
    User() {}
    User(QString username, QString passhash) : username(username), passhash(passhash) {}

    void setId(int id);
    void setUsername(QString username);
    void setPasshash(QString passhash);

    int getId() const;
    QString getUsername() const;
    QString getPasshash() const;
};

#endif // USER_H
