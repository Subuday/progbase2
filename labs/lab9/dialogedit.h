#ifndef DIALOGEDIT_H
#define DIALOGEDIT_H

#include <QDialog>
#include <QLineEdit>
#include <QListWidget>

#include "sqllite_storage.h"
#include "writer.hpp"

namespace Ui {
class DialogEdit;
}

class DialogEdit : public QDialog
{
    Q_OBJECT

public:
    explicit DialogEdit(SQLiteStorage * storage, int writer_id, QWidget *parent = nullptr);
    ~DialogEdit();

    void setEditId(QString id);
    void setEditName(QString name);
    void setEditNation(QString nation);
    void setEditBorn(QString born);
    void setEditDead(QString dead);

    Writer getWriter();

private slots:
    void updateWriter();

    void on_pushButtonAdd_clicked();

    void onListExtraItemClicked();

    void onListUserDataItemClicked();

    void on_pushButtonRemove_clicked();

private:
    Ui::DialogEdit *ui;

    SQLiteStorage * storage;

    Writer writer;

    QValidator* validator;

    QLineEdit* lineEditName;
    QLineEdit* lineEditNation;
    QLineEdit* lineEditBorn;
    QLineEdit* lineEditDead;

    QPushButton* pushButtonAdd;
    QPushButton* pushButtonRemove;

    QListWidget* listUserData;
    QListWidget* listExtra;

    void loadDataToUserList();
    void loadDataToExtraList();
};

#endif // DIALOGEDIT_H
