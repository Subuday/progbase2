#include "dialogedit.h"
#include "ui_dialogedit.h"

#include <iostream>

DialogEdit::DialogEdit(SQLiteStorage* storage, int writer_id, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogEdit)
{
    ui->setupUi(this);

    setFixedSize(width(), height());

    this->storage = storage;

    writer.setId(writer_id);

    validator = new QIntValidator(0,9999,this);

    lineEditName = findChild<QLineEdit*>("lineEditName");
    lineEditNation = findChild<QLineEdit*>("lineEditNation");
    lineEditBorn = findChild<QLineEdit*>("lineEditBorn");
    lineEditBorn->setValidator(validator);
    lineEditDead = findChild<QLineEdit*>("lineEditDead");
    lineEditDead->setValidator(validator);

    pushButtonAdd = findChild<QPushButton*>("pushButtonAdd");
    pushButtonRemove = findChild<QPushButton*>("pushButtonRemove");

    listUserData =  findChild<QListWidget*>("listUserData");
    listExtra = findChild<QListWidget*>("listExtra");

    loadDataToUserList();
    loadDataToExtraList();

    connect(this, SIGNAL(accepted()), this, SLOT(updateWriter()));
    connect(ui->listExtra, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(onListExtraItemClicked()));
    connect(ui->listUserData, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(onListUserDataItemClicked()));
}

DialogEdit::~DialogEdit()
{
    listUserData->clear();
    listExtra->clear();
    delete ui;
}

void DialogEdit::setEditId(QString id)
{
    writer.setId(id.toInt());
}


void DialogEdit::setEditName(QString name)
{
    writer.setName(name);
    lineEditName->setText(name);
}

void DialogEdit::setEditNation(QString nation)
{
    writer.setNation(nation);
    lineEditNation->setText(nation);
}

void DialogEdit::setEditBorn(QString born)
{
    writer.setBorn(born.toInt());
    lineEditBorn->setText(born);
}

void DialogEdit::setEditDead(QString dead)
{
    writer.setDied(dead.toInt());
    lineEditDead->setText(dead);
}

Writer DialogEdit::getWriter()
{
    return writer;
}

void DialogEdit::updateWriter()
{
    writer.setName(lineEditName->text());
    writer.setNation(lineEditNation->text());
    writer.setBorn(lineEditBorn->text().toInt());
    writer.setDied(lineEditDead->text().toInt());
}

void DialogEdit::loadDataToUserList()
{
    for(auto it : storage->getAllWriterBooks(writer.getId()))
    {
        QVariant qVariant;
        qVariant.setValue(it);

        QListWidgetItem *qBookListItem = new QListWidgetItem();
        qBookListItem->setText(QString::number(it.getYear()) + " " + it.getName());
        qBookListItem->setData(Qt::UserRole, qVariant);

        listUserData->addItem(qBookListItem);
    }
}

void DialogEdit::loadDataToExtraList()
{
    for(auto it : storage->getAllBooks())
    {
        QVariant qVariant;
        qVariant.setValue(it);

        QListWidgetItem *qBookListItem = new QListWidgetItem();
        qBookListItem->setText(QString::number(it.getYear()) + " " + it.getName());
        qBookListItem->setData(Qt::UserRole, qVariant);

        listExtra->addItem(qBookListItem);
    }
}

void DialogEdit::on_pushButtonAdd_clicked()
{
    QListWidgetItem* item = listExtra->currentItem();

    QVariant qVariant = item->data(Qt::UserRole);
    Book book = qVariant.value<Book>();

    if(storage->insertWriterBook(writer.getId(), book.getId()))
    {
        QListWidgetItem *qBookListItem = new QListWidgetItem();
        qBookListItem->setText(QString::number(book.getYear()) + " " + book.getName());
        qBookListItem->setData(Qt::UserRole, qVariant);

        listUserData->addItem(qBookListItem);

    } else {
        QMessageBox::critical(nullptr, QObject::tr("Error"),
                   QObject::tr("Can't add object"), QMessageBox::Cancel);
    }
}

void DialogEdit::onListExtraItemClicked()
{
    pushButtonAdd->setEnabled(true);
    pushButtonRemove->setEnabled(false);
}

void DialogEdit::onListUserDataItemClicked()
{
    pushButtonAdd->setEnabled(false);
    pushButtonRemove->setEnabled(true);
}

void DialogEdit::on_pushButtonRemove_clicked()
{
    QListWidgetItem* item = listUserData->currentItem();

    QVariant qVariant = item->data(Qt::UserRole);
    Book book = qVariant.value<Book>();

    if(storage->removeWriterBook(writer.getId(), book.getId()))
    {
        listUserData->removeItemWidget(item);
        delete item;
    } else {
        QMessageBox::critical(nullptr, QObject::tr("Error"),
                   QObject::tr("Can't add object"), QMessageBox::Cancel);
    }
}
