#include "dialogadd.h"
#include "ui_dialogadd.h"

DialogAdd::DialogAdd(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogAdd)
{
    ui->setupUi(this);

    setFixedSize(width(), height());

    validator = new QIntValidator(0,9999,this);

    lineEditName = findChild<QLineEdit*>("lineEditName");
    lineEditNation = findChild<QLineEdit*>("lineEditNation");
    lineEditBorn = findChild<QLineEdit*>("lineEditBorn");
    lineEditBorn->setValidator(validator);
    lineEditDead = findChild<QLineEdit*>("lineEditDead");
    lineEditDead->setValidator(validator);

    connect(this, SIGNAL(accepted()), this, SLOT(updateWriter()));
}

DialogAdd::~DialogAdd()
{
    delete ui;
}

Writer DialogAdd::getWriter()
{
    return writer;
}

void DialogAdd::updateWriter()
{
    writer.setName(lineEditName->text());
    writer.setNation(lineEditNation->text());
    writer.setBorn(lineEditBorn->text().toInt());
    writer.setDied(lineEditDead->text().toInt());
}
