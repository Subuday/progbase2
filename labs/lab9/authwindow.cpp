#include "authwindow.h"
#include "ui_authwindow.h"

AuthWindow::AuthWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::AuthWindow)
{
    ui->setupUi(this);
    setFixedSize(width(), height());
    setWindowTitle("Login");

    lineEditUsername = findChild<QLineEdit*>("lineEditUsername");
    lineEditPassword = findChild<QLineEdit*>("lineEditPassword");
    buttonSign = findChild<QPushButton*>("pushButtonSign");

    connected = false;
    storage = new SQLiteStorage("/data");
    if(storage != nullptr)
    {
        connected = storage->load();
        buttonSign->setEnabled(true);
    }
    else QMessageBox::critical(nullptr, QObject::tr("Error"),
                               QObject::tr("Out of memory."), QMessageBox::Cancel);

}

AuthWindow::~AuthWindow()
{
    delete storage;
    delete ui;
}



void AuthWindow::on_pushButtonSign_clicked()
{
    string username = "";
    string password = "";

    username = lineEditUsername->text().toStdString();
    password = lineEditPassword->text().toStdString();

    optional<User> user_op = storage->getUserAuth(username, password);
    if(user_op)
    {
        User userData = user_op.value();
        emit login(userData, storage);
        this->close();
    }
    else
    {
        QMessageBox::information(nullptr, QObject::tr("Authorization"),
                                       QObject::tr("Incorrect username or password"), QMessageBox::Ok);
    }

}
