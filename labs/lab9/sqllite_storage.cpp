#include "sqllite_storage.h"
bool SQLiteStorage::load()
{
    string file_name = "/home/max/Desktop/writers";
    QString q_file_name = QString::fromStdString(file_name);
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(q_file_name);
    if(!db.open())
    {
        QMessageBox::critical(nullptr, QObject::tr("Cannot open database"),
                   QObject::tr("Unable to establish a database connection."), QMessageBox::Cancel);
        return false;
    }
    return true;
}

bool SQLiteStorage::startTransactions()
{
    return db.transaction();
}


bool SQLiteStorage::save()
{
    return db.commit();
}

bool SQLiteStorage::rallback()
{

    return db.rollback();
}

vector<Writer> SQLiteStorage::getAllWriters(void)
{
        QSqlQuery query;
        vector<Writer> writers;
        query.exec("SELECT * FROM writers");
        if(!query.exec())
        {
            QMessageBox::critical(nullptr, QObject::tr("Error"),
                       QObject::tr("Unable to load data base."), QMessageBox::Cancel);
            return writers;
        }
        while(query.next())
        {
            Writer writer;
            writer.setId(query.value(0).toInt());
            writer.setName(query.value(1).toString());
            writer.setNation(query.value(2).toString());
            writer.setBorn(query.value(3).toInt());
            writer.setDied(query.value(4).toInt());
            writers.push_back(writer);
        }
        return writers;
}

vector<Writer> SQLiteStorage::getAllUserWriters(int user_id)
{
    QSqlQuery query;
    vector<Writer> writers;
    query.prepare("SELECT * FROM writers WHERE user_id = :user_id");
    query.bindValue(":user_id", user_id);
    if(!query.exec())
    {
        QMessageBox::critical(nullptr, QObject::tr("Error"),
                   QObject::tr("Unable to load data base."), QMessageBox::Cancel);
        return writers;
    }
    while(query.next())
    {
        Writer writer;
        writer.setId(query.value(0).toInt());
        writer.setName(query.value(1).toString());
        writer.setNation(query.value(2).toString());
        writer.setBorn(query.value(3).toInt());
        writer.setDied(query.value(4).toInt());
        writer.setUserId(query.value(5).toInt());
        writers.push_back(writer);
    }
    return writers;
}

optional<Writer> SQLiteStorage::getWriterById(int writer_id)
{
    Writer writer;
    QSqlQuery query;
    query.prepare("SELECT * FROM writers WHERE id = :id");
    query.bindValue(":id", writer_id);
    if(!query.exec())
    {
        QMessageBox::critical(nullptr, QObject::tr("Error"),
                   QObject::tr("Some problem happened"), QMessageBox::Cancel);
        return nullopt;
    }
    if(query.next())
    {
        writer.setId(query.value(0).toInt());
        writer.setName(query.value(1).toString());
        writer.setNation(query.value(2).toString());
        writer.setBorn(query.value(3).toInt());
        writer.setDied(query.value(4).toInt());
        return writer;
    }
    QMessageBox::information(nullptr, QObject::tr("Attention"),
               QObject::tr("Writer wasn't found"), QMessageBox::Cancel);
    return nullopt;
}

bool SQLiteStorage::updateWriter(const Writer &writer)
{
    QSqlQuery query;
    query.prepare("UPDATE writers SET name = :name, nationality = :nationality, born = :born, "
                  "dead = :dead WHERE id = :id");
    query.bindValue(":id", writer.getId());
    query.bindValue(":name", writer.getName());
    query.bindValue(":nationality", writer.getNation());
    query.bindValue(":born", writer.getBorn());
    query.bindValue(":dead", writer.getDied());
    if(!query.exec())
    {
        QMessageBox::critical(nullptr, QObject::tr("Error"),
                   QObject::tr("Unable to delete writer."), QMessageBox::Cancel);
        return false;
    }
    return true;
}

bool SQLiteStorage::removeWriter(const Writer & writer)
{
    QSqlQuery query;
    query.prepare("DELETE FROM writers WHERE id = :id AND user_id = :user_id");
    query.bindValue(":id", writer.getId());
    query.bindValue(":user_id", writer.getUserId());
    if(!query.exec())
    {
        QMessageBox::critical(nullptr, QObject::tr("Error"),
                   QObject::tr("Unable to delete writer."), QMessageBox::Cancel);
        return false;
    }
    return true;
}

int SQLiteStorage::insertWriter(const Writer &writer)
{
    QSqlQuery query;
    query.prepare("INSERT INTO writers (name, nationality, born, dead, user_id) "
                  "VALUES (:name, :nationality, :born, :dead, :user_id)");
    query.bindValue(":name", writer.getName());
    query.bindValue(":nationality", writer.getNation());
    query.bindValue(":born", writer.getBorn());
    query.bindValue(":dead", writer.getDied());
    query.bindValue(":user_id", writer.getUserId());
    if(!query.exec())
    {
        QMessageBox::critical(nullptr, QObject::tr("Error"),
                   QObject::tr("Unable to add writer."), QMessageBox::Cancel);
        return -1;
    }
    return query.lastInsertId().toInt();
}

vector<Book> SQLiteStorage::getAllBooks(void)
{
    QSqlQuery query;
    vector<Book> books;
    query.exec("SELECT * FROM books");
    if(!query.exec())
    {
        QMessageBox::critical(nullptr, QObject::tr("Error"),
                   QObject::tr("Unable to load data base."), QMessageBox::Cancel);
        return books;
    }
    while(query.next())
    {
        Book book;
        book.setId(query.value(0).toInt());
        book.setName(query.value(1).toString());
        book.setGenre(query.value(2).toString());
        book.setYear(query.value(3).toInt());
        books.push_back(book);
    }
    return books;
}

optional<Book> SQLiteStorage::getBookById(int book_id)
{
    Book book;
    QSqlQuery query;
    query.prepare("SELECT * FROM books WHERE id = :id");
    query.bindValue(":id", book_id);
    if(!query.exec())
    {
        QMessageBox::critical(nullptr, QObject::tr("Error"),
                   QObject::tr("Some problem happened"), QMessageBox::Cancel);
        return nullopt;
    }
    if(query.next())
    {
        book.setId(query.value(0).toInt());
        book.setName(query.value(1).toString());
        book.setGenre(query.value(2).toString());
        book.setYear(query.value(3).toInt());
        return book;
    }
    QMessageBox::information(nullptr, QObject::tr("Attention"),
               QObject::tr("Writer wasn't found"), QMessageBox::Cancel);
    return nullopt;
}

bool SQLiteStorage::updateBook(const Book &book)
{
    QSqlQuery query;
    query.prepare("UPDATE books SET name = :name, genre = :genre, year = :year, "
                  "WHERE id = :id");
    query.bindValue(":id", book.getId());
    query.bindValue(":name", book.getName());
    query.bindValue(":genre", book.getGenre());
    query.bindValue(":year", book.getYear());
    if(!query.exec())
    {
        QMessageBox::critical(nullptr, QObject::tr("Error"),
                   QObject::tr("Unable to delete writer."), QMessageBox::Cancel);
        return false;
    }
    return true;
}

bool SQLiteStorage::removeBook(int book_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM books WHERE id = :id");
    query.bindValue(":id", book_id);
    if(!query.exec())
    {
        QMessageBox::critical(nullptr, QObject::tr("Error"),
                   QObject::tr("Unable to delete writer."), QMessageBox::Cancel);
        return false;
    }
    return true;
}

int SQLiteStorage::insertBook(const Book &book)
{
    QSqlQuery query;
    query.prepare("INSERT INTO books (name, genre, year) "
                  "VALUES (:name, :genre, :year)");
    query.bindValue(":name", book.getName());
    query.bindValue(":genre", book.getGenre());
    query.bindValue(":year", book.getYear());
    if(!query.exec())
    {
        QMessageBox::critical(nullptr, QObject::tr("Error"),
                   QObject::tr("Unable to add writer."), QMessageBox::Cancel);
        return -1;
    }
    return query.lastInsertId().toInt();
}

optional<User> SQLiteStorage::getUserAuth(string & username, string & passhash)
{
    QString qUsername = QString::fromStdString(username);
    QByteArray qPassword = QString::fromStdString(passhash).toUtf8();
    QByteArray qPasswordHashBytes = QCryptographicHash::hash(qPassword, QCryptographicHash::Md5);
    QString passwordHash = QString(qPasswordHashBytes.toHex());

    QSqlQuery query;
    query.prepare("SELECT * FROM users WHERE username = :username AND password_hash = :password_hash");
    query.bindValue(":username", qUsername);
    query.bindValue(":password_hash", passwordHash);
    if(!query.exec())
    {
        QMessageBox::critical(nullptr, QObject::tr("Error"),
                   QObject::tr("Some autization problems happened"), QMessageBox::Cancel);
        return nullopt;
    }
    if(query.next())
    {
        User user;
        user.setId(query.value("id").toInt());
        user.setUsername(query.value("username").toString());
        user.setPasshash(query.value("password_hash").toString());
        return user;
    }
    return nullopt;
}

vector<Book> SQLiteStorage::getAllWriterBooks(int writer_id)
{
    vector<Book> books;
    QSqlQuery query;
    query.prepare("SELECT books.id, books.name, books.genre, books.year FROM "
                  "books, links WHERE links.id_writer = :id_writer AND links.id_book = books.id");
    query.bindValue(":id_writer", writer_id);
    if(!query.exec())
    {
        QMessageBox::critical(nullptr, QObject::tr("Error"),
                   QObject::tr("Some problems happened"), QMessageBox::Cancel);
        return books;
    }

    while(query.next())
    {
        Book book;
        book.setId(query.value("id").toInt());
        book.setName(query.value("name").toString());
        book.setGenre(query.value("genre").toString());
        book.setYear(query.value("year").toInt());
        books.push_back(book);
    }
    return books;
}
bool SQLiteStorage::insertWriterBook(int writer_id, int book_id)
{
    QSqlQuery query;
    query.prepare("INSERT INTO links (id_writer, id_book) VALUES (:id_writer, :id_book)");
    query.bindValue(":id_writer", writer_id);
    query.bindValue(":id_book", book_id);
    if(!query.exec())
    {
        QMessageBox::critical(nullptr, QObject::tr("Error"),
                   QObject::tr("Some problems happened"), QMessageBox::Cancel);
        return false;
    }

    return true;
}
bool SQLiteStorage::removeWriterBook(int writer_id, int book_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM links WHERE id_writer = :id_writer AND id_book = :id_book");
    query.bindValue(":id_writer", writer_id);
    query.bindValue(":id_book", book_id);
    if(!query.exec())
    {
        QMessageBox::critical(nullptr, QObject::tr("Error"),
                   QObject::tr("Some problems happened"), QMessageBox::Cancel);
        return false;
    }
    return true;
}


