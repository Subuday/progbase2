#include "user.h"

void User::setId(int id)
{
    this->id = id;
}

void User::setUsername(QString username)
{
    this->username = username;
}

void User::setPasshash(QString passhash)
{
    this->getPasshash() = passhash;
}

int User::getId() const
{
    return id;
}

QString User::getUsername() const
{
    return username;
}

QString User::getPasshash() const
{
    return passhash;
}
