#ifndef AUTHWINDOW_H
#define AUTHWINDOW_H

#include <QMainWindow>
#include <QLineEdit>
#include <QPushButton>

#include "sqllite_storage.h"

namespace Ui {
class AuthWindow;
}

class AuthWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit AuthWindow(QWidget *parent = nullptr);
    ~AuthWindow();

private slots:
    void on_pushButtonSign_clicked();

signals:
    void login(User userData, SQLiteStorage* storage);

private:
    Ui::AuthWindow *ui;
    SQLiteStorage* storage;

    QLineEdit* lineEditUsername;
    QLineEdit* lineEditPassword;

    QPushButton* buttonSign;

    bool connected;
};

#endif // AUTHWINDOW_H
