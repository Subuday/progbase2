#include "mainwindow.h"
#include "authwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    AuthWindow b;

    QObject::connect(&b, SIGNAL(login(User, SQLiteStorage*)), &w, SLOT(setLoginData(User, SQLiteStorage*)));
    QObject::connect(&b, SIGNAL(login(User, SQLiteStorage*)), &w, SLOT(show()));

    b.show();

    return a.exec();
}
