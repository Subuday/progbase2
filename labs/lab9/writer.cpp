#include "writer.hpp"

bool Writer::operator == (const Writer& wr) const
{
    return this->Id == wr.getId();
}

void Writer::operator = (const Writer& wr)
{
    this->Id = wr.getId();
    this->Name = wr.getName();
    this->Nation = wr.getNation();
    this->Born = wr.getBorn();
    this->Died = wr.getDied();
}

void Writer::setId(int id) {Id = id;}
void Writer::setName(QString name) {Name = name;}
void Writer::setNation(QString nation) {Nation = nation;}
void Writer::setBorn(int born) {Born = born;}
void Writer::setDied(int died) {Died = died;}
void Writer::setUserId(int userId) {UserID = userId;}

int      Writer::getId() const {return Id;}
QString  Writer::getName() const  {return Name;}
QString  Writer::getNation() const  {return Nation;}
int      Writer::getBorn() const {return Born;}
int      Writer::getDied() const {return Died;}
int      Writer::getUserId() const {return UserID;}
