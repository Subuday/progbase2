#include "dialogdelete.h"
#include "ui_dialogdelete.h"

DialogDelete::DialogDelete(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogDelete)
{
    ui->setupUi(this);

    setFixedSize(width(), height());

    labelWrID = this->findChild<QLabel*>("labelWrID");
    labelWrName = this->findChild<QLabel*>("labelWrName");
}

DialogDelete::~DialogDelete()
{
    delete ui;
}

void DialogDelete::setLabelWrID(QString id)
{
    labelWrID->setText(id);
}

void DialogDelete::setLabelWrName(QString name)
{
    labelWrName->setText(name);
}
