// Компілювати за допомогою:
//valgrind --leak-check=yes  ./lab ../data.csv -b -o ../test.csv -n 1200^C

#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <stdbool.h>
#include <time.h>
#include <ctype.h> 
#include <string.h>
#include <assert.h>
#include <progbase.h> // Спрощений ввід і вивід даних у консоль
#include <unistd.h>  
#include <getopt.h>

#include <csv.h>
#include <map.h>
#include <list.h>
#include <tree.h>
#include <str.h>


typedef struct Writer Writer;
struct Writer
{
    int id;
    char* fullname;
    char* nation;
    int bDate;
    int dDate;
    char* book;
};

void test(int N, char* outFile);

StrStrMap * createWriterMap(Writer* writer);
void cleanupMaps(List* list);
void createTableMapfromTableList(List* table, List* tableMap);

int main(int argc, char *argv[]) {
    // Початок програми
    int opt;  
    int N = -1;
    char* inFile = "../data.csv";
    char* outFile = NULL;
    bool binaryTree = false;

    while((opt = getopt(argc, argv, "bn:o:")) != -1)  
    {  
        switch(opt)  
        {  
            case 'n': 
                N = atoi(optarg);
                if(N < 0) return EXIT_FAILURE; 
                break;
            case 'o':  
                outFile = optarg; 
                break; 
            case 'b':
                binaryTree = true;
                break; 
            case ':':   
                fprintf(stderr, "option needs a value\n"); 
                return EXIT_FAILURE;
                break;  
            case '?':   
                fprintf(stderr, "unknown option: \n"); 
                return EXIT_FAILURE;
                break;  
        }  
    }  
      
    // optind is for the extra arguments 
    // which are not parsed 
    // for(; optind < argc; optind++){      
    //     printf("extra arguments: %s\n", argv[optind]);  
    // }
    if(optind < argc) {
        inFile = argv[optind];
    }

    if(inFile != NULL) {
        FILE *fp;
        fp = fopen(inFile, "r");
        if(fp == NULL) {fprintf(stderr, "FILE ISN'T OPENED"); return EXIT_FAILURE;};

        char ch = '\0';
        Vec* vec = Vec_init(10);

        while((ch = fgetc(fp)) != EOF) {
            Vec_add(vec, ch); 
        }
        Vec_add(vec, '\0');

        char* str = Vec_toStr(vec);
        if(strlen(str) == 0) {Vec_deinit(vec); return 0;};
        char* header = Csv_getHeaderTableFromString(str);

        List table;
        List_init(&table);
        List tableMap;
        List_init(&tableMap);

        Csv_fillTableFromString(&table, str);
        createTableMapfromTableList(&table, &tableMap);
        Csv_clearTable(&table);
        List_deinit(&table);

        if(binaryTree == true) {
            BSTree* tree = BSTree_alloc();

            for(int i = 0; i < tableMap.size; ++i) {
                BSTree_insert(tree, tableMap.items[i]);
            }
            
            puts("================================================");
            BSTree_print(tree);
            puts("================================================");

            if(N != -1) {
                BSTree_delete(tree, N);
                puts("*************************************************");
                BSTree_print(tree);
                puts("*************************************************");
            }

            BSTree_free(tree);
        }

        Csv_printTable(&tableMap);
        puts("");

        char* test = Csv_createStringFromTableMap(&tableMap, N); 
        char* csvTabelStr = Csv_addHeaderToString(header, test);

        if(outFile != NULL) {
            FILE *fout;
            fout = fopen(outFile, "w");
            if(fout == NULL) {fprintf(stderr, "FILE ISN'T OPENED"); abort();};
            fprintf(fout, "%s", csvTabelStr);
            fclose(fout);
        } else {
            puts(csvTabelStr);
        }

        cleanupMaps(&tableMap);
        List_deinit(&tableMap);
        Vec_deinit(vec);
        free(csvTabelStr);
        fclose(fp);
    } else {
        test(N, outFile);
    }
    // Кінець програми
    return 0;
}

StrStrMap * createWriterMap(Writer* writer) {
    char * idValue = String_allocFromInt(writer->id);
    char * fullnameValue = String_allocCopy(writer->fullname);
    char * nationValue = String_allocCopy(writer->nation);
    char * bDateValue = String_allocFromInt(writer->bDate);
    char * dDateValue = String_allocFromInt(writer->dDate);
    char * bookValue = String_allocCopy(writer->book);
    //
    StrStrMap * map = StrStrMap_alloc();
    StrStrMap_add(map, "id", idValue);
    StrStrMap_add(map, "fullname", fullnameValue);
    StrStrMap_add(map, "Nationality", nationValue);
    StrStrMap_add(map, "Born", bDateValue);
    StrStrMap_add(map, "Died", dDateValue);
    StrStrMap_add(map, "Famous book", bookValue);

    //
    assert(StrStrMap_contains(map, "id"));
    assert(StrStrMap_contains(map, "fullname"));
    assert(StrStrMap_contains(map, "Nationality"));
    assert(StrStrMap_contains(map, "Born"));
    assert(StrStrMap_contains(map, "Died"));
    assert(StrStrMap_contains(map, "Famous book"));
    //
    return map;
}

void cleanupMaps(List* list) {
    for(int i = 0; i < list->size; ++i) {
        StrStrMap_clear((StrStrMap *)list->items[i]);
        Map_free((StrStrMap *)list->items[i]);
    }
}

void test(int N, char* outFile) {

    struct Writer writers[] = {   
            {65,"Taras Shevchenko",     "Ukrainian",    1814, 1861, "Kobzar"},
            {98,"Ernest Hemingway",     "American",     1899, 1961, "\"The Old, Man and the Sea\""},
            {75,"Arthur Conan Doyle",   "Scottish",     1899, 1930, "\"Stories of Sherlock Holmes\""},
            {100,"Heinrich Heine",       "German",       1859, 1856, "Book of Songs"},
            {23,"Dante Alighieri",      "Italian",      1265, 1321, "The divine comedy"}
        };

        int stLength = sizeof(writers) / sizeof(writers[0]);
        List table; 
        List_init(&table);

        BSTree* tree = BSTree_alloc();

        for(int i = 0; i < stLength; ++i) {
            Writer *writer = &writers[i];
            StrStrMap * writerMap = createWriterMap(writer);
            List_add(&table, writerMap);     
            BSTree_insert(tree, writerMap); 
        }
        Csv_printTable(&table);
        puts("");

        BSTree_print(tree);
        puts("");

        BSTree_delete(tree, 1815);

        puts("------------------------------");
        BSTree_print(tree);
        puts("");
        

        char* csvString = Csv_createStringFromTableMap(&table, N);
        
        if(outFile != NULL) {
            FILE *fout;
                fout = fopen(outFile, "w");
                if(fout == NULL) {fprintf(stderr, "FILE ISN'T OPENED"); abort();};
                fprintf(fout, "%s", csvString);
                fclose(fout);
        } else {
            puts(csvString);
        }

        free(csvString);
        cleanupMaps(&table);
        List_deinit(&table);
        BSTree_free(tree);
}

void createTableMapfromTableList(List* table, List* tableMap) {
    for(int i = 0; i < table->size; ++i) {
        List* list = (List*)table->items[i];
        StrStrMap* map = StrStrMap_alloc();
        for(int j = 0; j < list->size; j++) {
            char* value = (char*)list->items[j];
            int lenValue = strlen(value);
            char* newVal = malloc(sizeof(char) * (lenValue + 1));
            strcpy(newVal, value);

            switch(j) {
                case 0: {
                    StrStrMap_add(map, "id", newVal);
                    break;
                }

                case 1: {
                    StrStrMap_add(map, "fullname", newVal);
                    break;
                }

                case 2: {
                    StrStrMap_add(map, "Nationality", newVal);
                    break;
                }

                case 3: {
                    StrStrMap_add(map, "Born", newVal);
                    break;
                }
                
                case 4: {
                    StrStrMap_add(map, "Died", newVal);
                    break;
                }

                case 5: {
                    StrStrMap_add(map, "Famous book", newVal);
                    break;
                }
            }
        }
        List_add(tableMap, map);
    }
}