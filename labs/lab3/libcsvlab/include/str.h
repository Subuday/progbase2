#pragma once 
#include <stdlib.h>
#include <stdio.h> 
#include <string.h>
#include <math.h> 

char* String_allocFromInt(int value);
char* String_allocCopy(char* str);
char* String_allocFromDouble(double value);