#pragma once

#include <stdlib.h>
#include <stdio.h> 
#include <string.h>
#include <stdbool.h>

#include "map.h"

typedef struct BSTree BSTree;
typedef struct BinTree BinTree;

struct BSTree {
    BinTree * root;  // a pointer to the root tree node
    size_t size;     // store number of added items to make _size() O(1)
};

BSTree* BSTree_alloc();
void BSTree_free(BSTree* self);
void BSTree_insert (BSTree * self, StrStrMap* value);
void BSTree_print(BSTree * self);
void BSTree_delete (BSTree * self, int key);
int BSTree_size(BSTree* self); 







