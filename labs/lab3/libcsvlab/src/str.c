#include "str.h"

char* String_allocFromInt(int value) {
    char* num = malloc(sizeof(char) * 30);
    if(num == NULL) {fprintf(stderr, "String_allocFromInt: Memory allocate NULL"); abort();}
    sprintf(num, "%d", value);
    return num;
}

char* String_allocCopy(char* str) {
    int strLen = strlen(str);
    char* copy =  malloc(sizeof(char) * (strLen + 1));
    if(copy == NULL) {fprintf(stderr, "String_allocCopy: Memory allocate NULL"); abort();}
    strcpy(copy, str);
    return copy;
}

char* String_allocFromDouble(double value) {
    char* doubleNum = malloc(sizeof(char) * 100);
    if(doubleNum == NULL) {fprintf(stderr, "String_allocFromDouble: Memory allocate NULL"); abort();}
    sprintf(doubleNum, "%f", value);
    return doubleNum;
}