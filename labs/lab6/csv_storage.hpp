#pragma once

#include <QString>
#include <QFile>
#include <QTextStream>

#include <iostream>  
#include <cstdlib> 
#include <vector>
#include <string>

#include "storage.hpp"
#include "optional.hpp"
#include "writer.hpp"
#include "book.hpp"
#include "csv.hpp"

using namespace std;

class CsvStorage : public Storage
{
  const string dir_name_;

  vector<Writer> writers_;
  vector<Book> books_;

  static Writer rowToWriter(const CsvRow &row);
  static CsvRow writerToRow(const Writer &wr);
  static Book rowToBook(const CsvRow &row);
  static CsvRow bookToRow(const Book &bk);

  int getNewWriterId();
  int getNewBookId();

 public:
   CsvStorage(const string & dir_name) : dir_name_(dir_name) { }
   ~CsvStorage() override {}

   bool load() override;
   bool save() override;
   // writers
   vector<Writer> getAllWriters(void) override;
   optional<Writer> getWriterById(int writer_id) override;
   bool updateWriter(const Writer &writer) override;
   bool removeWriter(int writer_id) override;
   int insertWriter(const Writer &writer) override;
   //books
   vector<Book> getAllBooks(void) override;
   optional<Book> getBookById(int book_id) override;
   bool updateBook(const Book &book) override;
   bool removeBook(int book_id) override;
   int insertBook(const Book &book) override;
};
