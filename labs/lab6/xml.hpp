#pragma once

#include <vector>
#include <string>

using std::vector;
using std::string;

using XmlElem = vector<string>;
using XmlTable = vector<XmlElem>;

namespace Xml
{
// parse csv, create & fill string table
XmlTable createTableFromString(const string &xml_str);
// return a string filled with csv from string table
string createStringFromTable(const XmlTable &xml_table);
}
