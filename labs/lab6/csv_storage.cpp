#include "csv_storage.hpp"

bool CsvStorage::load()
{
    string csv_str;
    string csv_str_extra;

    string file_name = ":/storage/csv" + dir_name_ + "/writers.csv";
    QString q_file_name = QString::fromStdString(file_name);
    QFile q_file(q_file_name);
    if(!q_file.open(QIODevice::ReadOnly | QIODevice::Text)) {return false;}
    QByteArray q_file_bytes = q_file.readAll();
    csv_str = q_file_bytes.toStdString();
    q_file.close();

    string file_name_extra = ":/storage/csv" + dir_name_ + "/books.csv";
    QString q_file_name_extra = QString::fromStdString(file_name_extra);
    QFile q_file_extra(q_file_name_extra);
    if(!q_file_extra.open(QIODevice::ReadOnly | QIODevice::Text)) {return false;}
    QByteArray q_file_bytes_extra = q_file_extra.readAll();
    csv_str_extra = q_file_bytes_extra.toStdString();
    q_file_extra.close();

    CsvTable csv_table = Csv::createTableFromString(csv_str);
    CsvTable csv_table_extra = Csv::createTableFromString(csv_str_extra);

    try
    {   
        //bool skip = true;
        for(auto it : csv_table) 
        {
            //if(skip) { skip = false; continue;}
            Writer writer = rowToWriter(it);
            writers_.push_back(writer);
        }

        //skip = true;
        for(auto it : csv_table_extra)
        {
            //if(skip) { skip = false; continue;}
            Book book = rowToBook(it);
            books_.push_back(book);
        }
    }
    catch(const std::invalid_argument& ia)
    {
        cerr << ia.what() << flush;
        return false;
    }

    return true;
}

Writer CsvStorage::rowToWriter(const CsvRow &row)
{
    Writer writer;

    try 
    {
        writer.setId(stoi(row.at(0)));
        writer.setName(row.at(1));
        writer.setNation(row.at(2));
        writer.setBorn(stoi(row.at(3)));
        writer.setDied(stoi(row.at(4)));
    } 
    catch (const std::invalid_argument& ia)
    {
        std::cerr << "Invalid argument: " << ia.what() << std::endl;
        throw ia;
    }
    
    return writer;
}

bool CsvStorage::save()
{

    string file_name = "." + dir_name_ + "/writers.csv";
    QString q_file_name = QString::fromStdString(file_name);
    QFile q_file(q_file_name);
    if(!q_file.open(QIODevice::ReadOnly | QIODevice::WriteOnly | QIODevice::Text)) {return false;}

    QTextStream out(&q_file);

    CsvTable csv_table;

    for(auto it : writers_)
    {
        CsvRow row = writerToRow(it);
        csv_table.push_back(row);
    }

    string csv_str = Csv::createStringFromTable(csv_table);
    QString q_csv_str = QString::fromStdString(csv_str);
    out << q_csv_str;
    q_file.close();

    string file_name_extra = "." + dir_name_ + "/books.csv";
    QString q_file_name_extra = QString::fromStdString(file_name_extra);
    QFile q_file_extra(q_file_name_extra);
    if(!q_file_extra.open(QIODevice::WriteOnly | QIODevice::Text)) {return false;}
    QTextStream out_extra(&q_file_extra);

    CsvTable csv_table_extra;

    for(auto it : books_)
    {
        CsvRow row = bookToRow(it);
        csv_table_extra.push_back(row);
    }

    string csv_str_extra = Csv::createStringFromTable(csv_table_extra);
    QString q_csv_str_extra = QString::fromStdString(csv_str_extra);
    out_extra << q_csv_str_extra;
    q_file_extra.close();

    return true;
}

CsvRow CsvStorage::writerToRow(const Writer &wr)
{
    CsvRow row;
    row.push_back(to_string(wr.getId()));
    row.push_back(wr.getName());
    row.push_back(wr.getNation());
    row.push_back(to_string(wr.getBorn()));
    row.push_back(to_string(wr.getDied()));

    return row;
}

vector<Writer> CsvStorage::getAllWriters(void)
{
    return writers_;
}

optional<Writer> CsvStorage::getWriterById(int writer_id)
{
    for(auto & it : writers_)
    {
        if(it.getId() == writer_id)
        {
            return it;
        }
    }

    return nullopt;
}

bool CsvStorage::updateWriter(const Writer &writer)
{
    for(auto &it : writers_)
    {
        if(it == writer)
        {
            it = writer;
            return true;
        }
    }
    return false;
}

bool CsvStorage::removeWriter(int writer_id)
{
    int i = 0;
    for(auto &it : writers_)
    {
        if(it.getId() == writer_id)
        {
            writers_.erase(writers_.begin() + i);
            return true;
        }
        ++i;
    }
    return false;
}

int CsvStorage::insertWriter(const Writer &writer)
{
    int id = getNewWriterId();
    Writer wrWithNewId = writer;
    wrWithNewId.setId(id);
    writers_.push_back(wrWithNewId);
    return id;
}

int CsvStorage::getNewWriterId()
{
    if(!writers_.empty())
    {
        Writer writer = writers_.back();
        int id = writer.getId();
        return ++id;
    }
    return 1;
}

Book CsvStorage::rowToBook(const CsvRow &row)
{
    Book book;

    try 
    {
        book.setId(stoi(row.at(0)));
        book.setName(row.at(1));
        book.setGenre(row.at(2));
        book.setYear(stoi(row.at(3)));
    } 
    catch (const std::invalid_argument& ia)
    {
        std::cerr << "Invalid argument: " << ia.what() << std::endl;
        throw ia;
    }
    
    return book;
}

CsvRow CsvStorage::bookToRow(const Book &bk)
{
    CsvRow row;
    row.push_back(to_string(bk.getId()));
    row.push_back(bk.getName());
    row.push_back(bk.getGenre());
    row.push_back(to_string(bk.getYear()));

    return row;
}

vector<Book> CsvStorage::getAllBooks(void)
{
    return books_;
}

optional<Book> CsvStorage::getBookById(int book_id)
{
    for(auto & it : books_)
    {
        if(it.getId() == book_id)
        {
            return it;
        }
    }

    return nullopt;
}

bool CsvStorage::updateBook(const Book &book)
{
    for(auto &it : books_)
    {
        if(it == book)
        {
            it = book;
            return true;
        }
    }
    return false;
}

bool CsvStorage::removeBook(int book_id)
{
    int i = 0;
    for(auto &it : books_)
    {
        if(it.getId() == book_id)
        {
            books_.erase(books_.begin() + i);
            return true;
        }
        ++i;
    }
    return false;
}

int CsvStorage::insertBook(const Book &book)
{
    int id = getNewBookId();
    Book bkWithNewId = book;
    bkWithNewId.setId(id);
    books_.push_back(bkWithNewId);
    return id;
}

int CsvStorage::getNewBookId()
{
    if(!books_.empty())
    {
        Book book = books_.back();
        int id = book.getId();
        return ++id;
    }
    return 1;
}
