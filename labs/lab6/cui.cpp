#include "cui.hpp"

void Cui::show()
{
    while(true)
    {
        cout << "1. Writers." << endl;
        cout << "2. Books." << endl;
        cout << "3. Exit" << endl;

        char num;
        cout << "Enter number: ";
        cin >> num;

        switch(num)
        {
            case '1':
            {
                cout << "\033[2J\033[1;1H";
                writersMainMenu();
                break;
            }

            case '2':
            {
                cout << "\033[2J\033[1;1H";
                booksMainMenu();
                break;
            }

            case '3':
            {
                storage_->save();
                cout << "\033[2J\033[1;1H";
                cout << "Bye - bye!:)" << endl;
                return;
            }

            default:
            {
                cout << "\033[2J\033[1;1H";
                cout << "Try again!" << endl;
                break;
            }
        }
    }
}

void Cui::writersMainMenu()
{
    while(true)
    {

        cout << "1. Show all writer." << endl;
        cout << "2. Choose writer by ID." << endl;
        cout << "3. Add writer." << endl;
        cout << "4. Back" << endl;

        char num;
        cout << "Enter number: ";
        cin >> num;
        
        switch(num)
        {
            case '1':
            {
                cout << "\033[2J\033[1;1H";
                writersMenu();
                break;
            }
            case '2':
            {   
                int id = 0;
                cout << "\033[2J\033[1;1H";
                cout << "Enter ID: ";
                cin >> id;
                optional<Writer> writer_opt = storage_->getWriterById(id);
                if(writer_opt)
                {
                    writerMenu(id);
                }
                else
                {
                    cout << "Writer wasn't found:(" << endl;
                    cout << "Try again." << endl;
                }
                break;
            }
            case '3':
            {
                cout << "\033[2J\033[1;1H";
                writerCreateMenu();
                break;
            }

            case '4':
            {   
                storage_->save();
                cout << "\033[2J\033[1;1H";
                return;
            }

            default:
            {   
                cout << "\033[2J\033[1;1H";
                cout << "Try again!" << endl;
                break;
            }
        }
    }
}

void Cui::writersMenu()
{
    while(true)
    {

        for(auto it : storage_->getAllWriters())
        {
            cout << "ID: " << it.getId() << " Name: " << it.getName() << endl;
        }

        cout << "1. Choose writer by ID." << endl;
        cout << "2. Add writer." << endl;
        cout << "3. Back" << endl;

        char num;
        cout << "Enter number: ";
        cin >> num;
        
        switch(num)
        {
            case '1':
            {   
                int id = 0;
                cout << "\033[2J\033[1;1H";
                cout << "Enter ID: ";
                cin >> id;
                optional<Writer> writer_opt = storage_->getWriterById(id);
                if(writer_opt)
                {
                    writerMenu(id);
                }
                else
                {
                    cout << "Writer wasn't found:(" << endl;
                    cout << "Try again." << endl;
                }
                break;
            }
            case '2':
            {
                cout << "\033[2J\033[1;1H";
                writerCreateMenu();
                break;
            }

            case '3':
            {   
                cout << "\033[2J\033[1;1H";
                return;
            }

            default:
            {
                cout << "\033[2J\033[1;1H";
                cout << "Try again!" << endl;
                break;
            }
        }
    }
}

void Cui::writerCreateMenu()
{
    string name;
    string nation;
    string born;
    string died;
    
    while(true)
    {
        cout << "Let's start creating new writer." << endl;
        cout << "1. Create new writer." << endl;
        cout << "2. Back" << endl;

        char num;
        cout << "Enter number: ";
        cin >> num;

        switch(num)
        {
            case '1':
            {
                cout << "Enter writer's name: ";
                cin >> name;

                cout << "Enter writer's nationality: ";
                cin >> nation;

                cout << "Enter writer's date of the birth: ";
                cin >> born;

                cout << "Enter writer's date of the death: ";
                cin >> died;

                try 
                {
                    Writer writer;
                    writer.setId(-1);
                    writer.setName(name);
                    writer.setNation(nation);
                    writer.setBorn(stoi(born));
                    writer.setDied(stoi(died));

                    storage_->insertWriter(writer);
                } 
                catch (const std::invalid_argument& ia)
                {
                    cout << "\033[2J\033[1;1H";
                    std::cerr << "Incorect input: " << ia.what() << std::endl;
                    return;
                }
                cout << "\033[2J\033[1;1H";
                cout << "Complete!" << endl;
                break;
            }

            case '2':
            {   
                cout << "\033[2J\033[1;1H";
                return;
            }

            default:
            {   
                cout << "\033[2J\033[1;1H";
                cout << "Try again" << endl;
                break;
            }
            
        }
    }
}

void Cui::writerMenu(int entity_id)
{
    while(true)
    {
        cout << "1. Get detail information." << endl;
        cout << "2. Update information" << endl;
        cout << "3. Delete writer" << endl;
        cout << "4. Back" << endl;
        
        char num;
        cout << "Enter number: ";
        cin >> num;

        switch(num)
        {
            case '1':
            {
                cout << "\033[2J\033[1;1H";
                optional<Writer> writer_opt = storage_->getWriterById(entity_id);
                if(writer_opt)
                {
                    cout << endl;
                    Writer & writer = writer_opt.value();
                    cout << "ID: ";
                    cout << to_string(writer.getId()) << endl;
                    cout << "Name: ";
                    cout << writer.getName() << endl;
                    cout << "Nationality: ";
                    cout << writer.getNation() << endl;
                    cout << "The date of the bidth: ";
                    cout << writer.getBorn() << endl;
                    cout << "The date of the death: ";
                    cout << writer.getDied() << endl;
                    cout << endl;
                }
                break;
            }

            case '2':
            {
                
                cout << "\033[2J\033[1;1H";
                writerUpdateMenu(entity_id);
                break;
            }

            case '3':
            {
                writerDeleteMenu(entity_id);
                cout << "\033[2J\033[1;1H";
                return;
            }

            case '4':
            {   
                cout << "\033[2J\033[1;1H";
                return;
            }

            default:
            {
                cout << "\033[2J\033[1;1H";
                cout << "Try again!" << endl;
                break;
            }
        }
    }
}

void Cui::writerDeleteMenu(int entity_id)
{
    while(true)
    {
        cout << "Are you sure you want to delete a writer?" << endl;
        cout << "1 - YES, 2 - NO" << endl;

        char num;
        cout << "Enter number: ";
        cin >> num;

        switch (num)
        {
        case '1':
            cout << "\033[2J\033[1;1H";
            storage_->removeWriter(entity_id);
            cout << "Deleted!" << endl;
            return;

        case '2': 
            cout << "\033[2J\033[1;1H";
            return;
        
        default:
            cout << "Try again!" << endl;
            break;
        }

    }
}

void Cui::writerUpdateMenu(int entity_id)
{
    optional<Writer> writer_opt = storage_->getWriterById(entity_id);
    if(writer_opt)
    {   
        Writer & writer = writer_opt.value();

        while(true)
        {
            cout << "1. Update name." << endl;
            cout << "2. Update nationality." << endl;
            cout << "3. Update the date of the birth." << endl;
            cout << "4. Update the date of the death." << endl;
            cout << "5. Back" << endl;

            char num;
            cout << "Enter number: ";
            cin >> num;

            switch(num)
            {
                case '1':
                {   
                    cout << "\033[2J\033[1;1H";
                    cout << "Input new name: ";
                    string name;
                    cin >> name;
                    writer.setName(name);
                    cout << "\033[2J\033[1;1H";
                    cout << "Complete!" << endl;
                    break;
                }

                case '2':
                {
                    cout << "\033[2J\033[1;1H";
                    cout << "Input new nationality: ";
                    string nation;
                    cin >> nation;
                    writer.setNation(nation);
                    cout << "\033[2J\033[1;1H";
                    cout << "Complete!" << endl;
                    break;
                }

                case '3':
                {   
                    cout << "\033[2J\033[1;1H";
                    cout << "Input new the date of the birth: ";
                    string born;
                    cin >> born;
                    try
                    {
                        writer.setBorn(stoi(born));
                        cout << "\033[2J\033[1;1H";
                        cout << "Complete!" << endl;
                    } 
                    catch (const std::invalid_argument& ia)
                    {
                        cout << "\033[2J\033[1;1H";
                        std::cerr << "Incorect input!"<< ia.what() <<  std::endl;
                        cout << endl;
                        return;
                    }
                    break;
                }

                case '4':
                {
                    cout << "\033[2J\033[1;1H";
                    cout << "Input new the date of the death: ";
                    string died;
                    cin >> died;
                    try
                    {
                        writer.setDied(stoi(died));
                        cout << "\033[2J\033[1;1H";
                        cout << "Complete!" << endl;
                    } 
                    catch (const std::invalid_argument& ia)
                    {
                        cout << "\033[2J\033[1;1H";
                        std::cerr << "Incorect input!" << ia.what() << std::endl;
                        return;
                    }
                    break;
                }

                case '5':
                {
                    storage_->updateWriter(writer);
                    cout << "\033[2J\033[1;1H";
                    return;
                }

                default:
                {
                    cout << "\033[2J\033[1;1H";
                    cout << "Try again!" << endl;
                    break;
                }
            }
        }
    } 
    else 
    {
        cout << "\033[2J\033[1;1H";
        cout << "Some error happened. Try again!" << endl;
        return;
    }

}

void Cui::booksMainMenu()
{
    while(true)
    {

        cout << "1. Show all books." << endl;
        cout << "2. Choose book by ID." << endl;
        cout << "3. Add book." << endl;
        cout << "4. Back" << endl;

        char num;
        cout << "Enter number: ";
        cin >> num;
        
        switch(num)
        {
            case '1':
            {
                cout << "\033[2J\033[1;1H";
                booksMenu();
                break;
            }
            case '2':
            {   
                int id = 0;
                cout << "\033[2J\033[1;1H";
                cout << "Enter ID: ";
                cin >> id;
                optional<Book> book_opt = storage_->getBookById(id);
                if(book_opt)
                {
                    bookMenu(id);
                }
                else
                {
                    cout << "Book wasn't found:(" << endl;
                    cout << "Try again." << endl;
                }
                break;
            }
            case '3':
            {
                cout << "\033[2J\033[1;1H";
                bookCreateMenu();
                break;
            }

            case '4':
            {   
                storage_->save();
                cout << "\033[2J\033[1;1H";
                return;
            }

            default:
            {   
                cout << "\033[2J\033[1;1H";
                cout << "Try again!" << endl;
                break;
            }
        }
    }
}

void Cui::booksMenu()
{
    while(true)
    {

        for(auto it : storage_->getAllBooks())
        {
            cout << "ID: " << it.getId() << " Name: " << it.getName() << endl;
        }

        cout << "1. Choose book by ID." << endl;
        cout << "2. Add book." << endl;
        cout << "3. Back" << endl;

        char num;
        cout << "Enter number: ";
        cin >> num;
        
        switch(num)
        {
            case '1':
            {   
                int id = 0;
                cout << "\033[2J\033[1;1H";
                cout << "Enter ID: ";
                cin >> id;
                optional<Book> book_opt = storage_->getBookById(id);
                if(book_opt)
                {
                    bookMenu(id);
                }
                else
                {
                    cout << "Book wasn't found:(" << endl;
                    cout << "Try again." << endl;
                }
                break;
            }
            case '2':
            {
                cout << "\033[2J\033[1;1H";
                bookCreateMenu();
                break;
            }

            case '3':
            {   
                cout << "\033[2J\033[1;1H";
                return;
            }

            default:
            {
                cout << "\033[2J\033[1;1H";
                cout << "Try again!" << endl;
                break;
            }
        }
    }
}

void Cui::bookCreateMenu()
{
    string name;
    string genre;
    string year;
    
    while(true)
    {
        cout << "Let's start creating new book." << endl;
        cout << "1. Create new book." << endl;
        cout << "2. Back" << endl;

        char num;
        cout << "Enter number: ";
        cin >> num;

        switch(num)
        {
            case '1':
            {
                cout << "Enter book's name: ";
                cin >> name;

                cout << "Enter book's genre: ";
                cin >> genre;

                cout << "Enter books's year: ";
                cin >> year;

                try 
                {
                    Book book;
                    book.setId(-1);
                    book.setName(name);
                    book.setGenre(genre);
                    book.setYear(stoi(year));

                    storage_->insertBook(book);
                } 
                catch (const std::invalid_argument& ia)
                {
                    cout << "\033[2J\033[1;1H";
                    std::cerr << "Incorect input: " << ia.what() << std::endl;
                    return;
                }
                cout << "\033[2J\033[1;1H";
                cout << "Complete!" << endl;
                break;
            }

            case '2':
            {   
                cout << "\033[2J\033[1;1H";
                return;
            }

            default:
            {   
                cout << "\033[2J\033[1;1H";
                cout << "Try again" << endl;
                break;
            }  
        }
    }
}

void Cui::bookMenu(int entity_id)
{
    while(true)
    {
        cout << "1. Get detail information." << endl;
        cout << "2. Update information" << endl;
        cout << "3. Delete book" << endl;
        cout << "4. Back" << endl;
        
        char num;
        cout << "Enter number: ";
        cin >> num;

        switch(num)
        {
            case '1':
            {
                cout << "\033[2J\033[1;1H";
                optional<Book> book_opt = storage_->getBookById(entity_id);
                if(book_opt)
                {
                    cout << endl;
                    Book & book = book_opt.value();
                    cout << "ID: ";
                    cout << to_string(book.getId()) << endl;
                    cout << "Name: ";
                    cout << book.getName() << endl;
                    cout << "Genre: ";
                    cout << book.getGenre() << endl;
                    cout << "Year: ";
                    cout << book.getYear() << endl;
                    cout << endl;
                }
                break;
            }

            case '2':
            {
                
                cout << "\033[2J\033[1;1H";
                bookUpdateMenu(entity_id);
                break;
            }

            case '3':
            {
                bookDeleteMenu(entity_id);
                cout << "\033[2J\033[1;1H";
                return;
            }

            case '4':
            {   
                cout << "\033[2J\033[1;1H";
                return;
            }

            default:
            {
                cout << "\033[2J\033[1;1H";
                cout << "Try again!" << endl;
                break;
            }
        }
    }
}

void Cui::bookDeleteMenu(int entity_id)
{
    while(true)
    {
        cout << "Are you sure you want to delete a book?" << endl;
        cout << "1 - YES, 2 - NO" << endl;

        char num;
        cout << "Enter number: ";
        cin >> num;

        switch (num)
        {
        case '1':
            cout << "\033[2J\033[1;1H";
            storage_->removeBook(entity_id);
            cout << "Deleted!" << endl;
            return;

        case '2': 
            cout << "\033[2J\033[1;1H";
            return;
        
        default:
            cout << "Try again!" << endl;
            break;
        }

    }
}

void Cui::bookUpdateMenu(int entity_id)
{
    optional<Book> book_opt = storage_->getBookById(entity_id);
    if(book_opt)
    {   
        Book & book = book_opt.value();

        while(true)
        {
            cout << "1. Update name." << endl;
            cout << "2. Update genre." << endl;
            cout << "3. Update year." << endl;
            cout << "4. Back" << endl;

            char num;
            cout << "Enter number: ";
            cin >> num;

            switch(num)
            {
                case '1':
                {   
                    cout << "\033[2J\033[1;1H";
                    cout << "Input new name: ";
                    string name;
                    cin >> name;
                    book.setName(name);
                    cout << "\033[2J\033[1;1H";
                    cout << "Complete!" << endl;
                    break;
                }

                case '2':
                {
                    cout << "\033[2J\033[1;1H";
                    cout << "Input new nationality: ";
                    string genre;
                    cin >> genre;
                    book.setGenre(genre);
                    cout << "\033[2J\033[1;1H";
                    cout << "Complete!" << endl;
                    break;
                }

                case '3':
                {   
                    cout << "\033[2J\033[1;1H";
                    cout << "Input new year: ";
                    string year;
                    cin >> year;
                    try
                    {
                        book.setYear(stoi(year));
                        cout << "\033[2J\033[1;1H";
                        cout << "Complete!" << endl;
                    } 
                    catch (const std::invalid_argument& ia)
                    {
                        cout << "\033[2J\033[1;1H";
                        std::cerr << "Incorect input!"<< ia.what() << std::endl;
                        cout << endl;
                        return;
                    }
                    break;
                }

                case '4':
                {
                    storage_->updateBook(book);
                    cout << "\033[2J\033[1;1H";
                    return;
                }

                default:
                {
                    cout << "\033[2J\033[1;1H";
                    cout << "Try again!" << endl;
                    break;
                }
            }
        }
    } 
    else 
    {
        cout << "\033[2J\033[1;1H";
        cout << "Some error happened. Try again!" << endl;
        return;
    }
}

