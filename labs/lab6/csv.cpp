#include "csv.hpp"

CsvTable Csv::createTableFromString(const string &csv_str) 
{
    CsvTable table;

    for(auto it = csv_str.begin(); it != csv_str.end(); ++it)
    {
        CsvRow row;
        string str = "";
        bool flag = true;
        
        while(flag)
        {   

            switch (*it)
            {
                case '\"': 
                {  
                    ++it;
                    while(true)
                    {
                        if(((it + 1) == csv_str.end() && *it == '\"')|| (*(it+1) == ',' && *it == '\"') || 
                        (*(it+1) == '\n' && *it == '\"')) break;
                        else if(*it == '\"' && *(it + 1) == '\"')
                        {
                            str += *it;
                            it += 2;
                            continue;
                        }
                        str += *it;
                        ++it;
                    }

                    if(it + 1 == csv_str.end()) {
                        row.push_back(str);
                        flag = false;
                        break;
                    } 
                    ++it;
                    break;
                }

                case ',':
                {
                    row.push_back(str);
                    str = "";
                    ++it;
                    break;
                }
                
                case '\n':
                {   
                    row.push_back(str);
                    flag = false;
                    break;
                }

                default:
                {
                    str += *it;
                    if(it + 1 == csv_str.end()) 
                    { 
                        row.push_back(str);
                        flag = false;
                        break;
                    }
                     ++it;
                    break;
                }
            }
        }
        table.push_back(row);
    }

    return table;
}

static string strToCsv(const string & str)
{
    string csv_str = "";
    
    bool brackets = false;

    for(auto it : str)
    {
        if(it == ',' || it == '\n' || it == '\"') {
            brackets = true;
        }
    }

    if(brackets == false && str != "") return str;

    csv_str += "\"";

    for(auto it : str)
    {
        if(it == '\"') csv_str += it;
        csv_str += it;
    }

    csv_str += "\"";
    

    return csv_str;

}

string Csv::createStringFromTable(const CsvTable &csv_table)
{
    string str;
    for(auto it = csv_table.begin(); it != csv_table.end(); ++it)
    {
        CsvRow row = *it;

        for(auto el = row.begin(); el != row.end(); ++el)
        {
            string strColum = *el;
            string csv_str = strToCsv(strColum);
            str += csv_str;
            if(el + 1 != row.end())str += ",";
        }

        if(it + 1 != csv_table.end())str += "\n";
    }
    return str;
}
