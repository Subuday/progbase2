QT -= gui
QT += xml

CONFIG += c++17 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
    book.cpp \
    csv.cpp \
    cui.cpp \
    writer.cpp \
    csv_storage.cpp \
    xml_storage.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    book.hpp \
    csv.hpp \
    cui.hpp \
    optional.hpp \
    writer.hpp \
    csv_storage.hpp \
    storage.hpp \
    xml_storage.hpp

RESOURCES += \
    data.qrc

DISTFILES += \
    data/books.csv \
    data/writers.csv \
    data/writers_x.xml \
    data/books_x.xml \
    data/test.xml
