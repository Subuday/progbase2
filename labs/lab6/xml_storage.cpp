#include "xml_storage.hpp"

bool XmlStorage::load()
{
    string file_name = ":/storage/xml" + dir_name_ + "/writers_x.xml";
    optional<vector<Writer>> opt_writets = xmlToWriters(file_name);
    if(opt_writets)
    {
        writers_ = opt_writets.value();
    }
    else
    {
        cerr << "File wasn't found." << flush;
        return false;
    }

    string file_name_extra = ":/storage/xml" + dir_name_ + "/books_x.xml";
    optional<vector<Book>> opt_books = xmlToBooks(file_name_extra);
    if(opt_books)
    {
        books_ = opt_books.value();
    }
    else
    {
        cerr << "File wasn't found." << flush;
        return false;
    }

    return true;
}

optional<vector<Writer>> XmlStorage::xmlToWriters(const string file_path)
{
    vector <Writer> writers;
    QDomDocument xmlReader;

    QString q_file_name = QString::fromStdString(file_path);
    QFile q_file(q_file_name);
    if(!q_file.open(QIODevice::ReadOnly | QIODevice::Text)) {return nullopt;}
    xmlReader.setContent(&q_file);
    q_file.close();

    QDomElement root = xmlReader.documentElement();
    QDomElement component = root.firstChild().toElement();

    while(!component.isNull())
    {
        if(component.tagName() == "writer")
        {
            QDomElement child = component.firstChild().toElement();

            Writer writer;

            QString id;
            QString name;
            QString nation;
            QString born;
            QString dead;

            while(!child.isNull())
            {
                if(child.tagName() == "Id") id = child.firstChild().toText().data();
                if(child.tagName() == "Name") name = child.firstChild().toText().data();
                if(child.tagName() == "Nationality") nation = child.firstChild().toText().data();
                if(child.tagName() == "Born") born = child.firstChild().toText().data();
                if(child.tagName() == "Dead") dead = child.firstChild().toText().data();

                child = child.nextSibling().toElement();
            }

            try
            {
                writer.setId(stoi(id.toStdString()));
                writer.setName(name.toStdString());
                writer.setNation(nation.toStdString());
                writer.setBorn(stoi(born.toStdString()));
                writer.setDied(stoi(dead.toStdString()));
            }
            catch (const std::invalid_argument& ia)
            {
                std::cerr << "Invalid argument: " << ia.what() << std::endl;
                throw ia;
            }

            writers.push_back(writer);
        }
        component = component.nextSibling().toElement();
    }

    return writers;
}

optional<vector<Book>> XmlStorage::xmlToBooks(const string file_path)
{
    vector <Book> books;
    QDomDocument xmlReader;

    QString q_file_name = QString::fromStdString(file_path);
    QFile q_file(q_file_name);
    if(!q_file.open(QIODevice::ReadOnly | QIODevice::Text)) {return nullopt;}
    xmlReader.setContent(&q_file);
    q_file.close();

    QDomElement root = xmlReader.documentElement();
    QDomElement component = root.firstChild().toElement();

    while(!component.isNull())
    {
        if(component.tagName() == "book")
        {
            QDomElement child = component.firstChild().toElement();

            Book book;

            QString id;
            QString name;
            QString genre;
            QString year;

            while(!child.isNull())
            {
                if(child.tagName() == "Id") id = child.firstChild().toText().data();
                if(child.tagName() == "Name") name = child.firstChild().toText().data();
                if(child.tagName() == "Genre") genre = child.firstChild().toText().data();
                if(child.tagName() == "Year") year = child.firstChild().toText().data();

                child = child.nextSibling().toElement();
            }

            try
            {
                book.setId(stoi(id.toStdString()));
                book.setName(name.toStdString());
                book.setGenre(genre.toStdString());
                book.setYear(stoi(year.toStdString()));
            }
            catch (const std::invalid_argument& ia)
            {
                std::cerr << "Invalid argument: " << ia.what() << std::endl;
                throw ia;
            }

            books.push_back(book);
        }
        component = component.nextSibling().toElement();
    }

    return books;
}


bool XmlStorage::save()
{
    try
    {
        writersToXml("/home/max/Desktop/test.xml", writers_);
        booksToXml("/home/max/Desktop/test2.xml", books_);
    }
    catch (const char* e)
    {
        cerr << e << flush;
        return false;
    }

    return true;
}

void XmlStorage::writersToXml(const string file_path, vector<Writer> &writers)
{
    QString q_file_name = QString::fromStdString(file_path);
    QFile q_file(q_file_name);
    if(!q_file.open(QIODevice::WriteOnly | QIODevice::Text)) {throw "Saving error.";}
    QXmlStreamWriter stream(&q_file);
    stream.setAutoFormatting(true);
    stream.writeStartDocument();
    stream.writeStartElement("writers");

    for(auto it : writers)
    {
        stream.writeStartElement("writer");
        stream.writeTextElement("Id", QString::number(it.getId()));
        stream.writeTextElement("Name", QString::fromStdString(it.getName()));
        stream.writeTextElement("Nationality", QString::fromStdString(it.getNation()));
        stream.writeTextElement("Born", QString::number(it.getBorn()));
        stream.writeTextElement("Dead", QString::number(it.getDied()));
        stream.writeEndElement();
    }
    stream.writeEndElement();
    q_file.close();
}

void XmlStorage::booksToXml(const string file_path, vector<Book> &books)
{
    QString q_file_name = QString::fromStdString(file_path);
    QFile q_file(q_file_name);
    if(!q_file.open(QIODevice::WriteOnly | QIODevice::Text)) {throw "Saving error.";}
    QXmlStreamWriter stream(&q_file);
    stream.setAutoFormatting(true);
    stream.writeStartDocument();
    stream.writeStartElement("books");

    for(auto it : books)
    {
        stream.writeStartElement("book");
        stream.writeTextElement("Id", QString::number(it.getId()));
        stream.writeTextElement("Name", QString::fromStdString(it.getName()));
        stream.writeTextElement("Genre", QString::fromStdString(it.getGenre()));
        stream.writeTextElement("Year", QString::number(it.getYear()));
        stream.writeEndElement();
    }
    stream.writeEndElement();
    q_file.close();
}

vector<Writer> XmlStorage::getAllWriters(void)
{
    return writers_;
}

optional<Writer> XmlStorage::getWriterById(int writer_id)
{
    for(auto & it : writers_)
    {
        if(it.getId() == writer_id)
        {
            return it;
        }
    }

    return nullopt;
}

bool XmlStorage::updateWriter(const Writer &writer)
{
    for(auto &it : writers_)
    {
        if(it == writer)
        {
            it = writer;
            return true;
        }
    }
    return false;
}

bool XmlStorage::removeWriter(int writer_id)
{
    int i = 0;
    for(auto &it : writers_)
    {
        if(it.getId() == writer_id)
        {
            writers_.erase(writers_.begin() + i);
            return true;
        }
        ++i;
    }
    return false;
}

int XmlStorage::insertWriter(const Writer &writer)
{
    int id = getNewWriterId();
    Writer wrWithNewId = writer;
    wrWithNewId.setId(id);
    writers_.push_back(wrWithNewId);
    return id;
}

int XmlStorage::getNewWriterId()
{
    if(!writers_.empty())
    {
        Writer writer = writers_.back();
        int id = writer.getId();
        return ++id;
    }
    return 1;
}

vector<Book> XmlStorage::getAllBooks(void)
{
    return books_;
}

optional<Book> XmlStorage::getBookById(int book_id)
{
    for(auto & it : books_)
    {
        if(it.getId() == book_id)
        {
            return it;
        }
    }

    return nullopt;
}

bool XmlStorage::updateBook(const Book &book)
{
    for(auto &it : books_)
    {
        if(it == book)
        {
            it = book;
            return true;
        }
    }
    return false;
}

bool XmlStorage::removeBook(int book_id)
{
    int i = 0;
    for(auto &it : books_)
    {
        if(it.getId() == book_id)
        {
            books_.erase(books_.begin() + i);
            return true;
        }
        ++i;
    }
    return false;
}

int XmlStorage::insertBook(const Book &book)
{
    int id = getNewBookId();
    Book bkWithNewId = book;
    bkWithNewId.setId(id);
    books_.push_back(bkWithNewId);
    return id;
}

int XmlStorage::getNewBookId()
{
    if(!books_.empty())
    {
        Book book = books_.back();
        int id = book.getId();
        return ++id;
    }
    return 1;
}
