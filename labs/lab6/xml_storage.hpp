#pragma once

#include <QtXml>

#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>

#include "storage.hpp"
#include "optional.hpp"
#include "writer.hpp"
#include "book.hpp"
#include "csv.hpp"

using namespace std;

class XmlStorage: public Storage
{
public:
    const string dir_name_;

    vector<Writer> writers_;
    vector<Book> books_;

    static optional<vector<Writer>> xmlToWriters(const string file_path);
    static optional<vector<Book>> xmlToBooks(const string file_path);
    static void writersToXml(const string file_path, vector<Writer> & writes);
    static void booksToXml(const string file_path, vector<Book> & books_);

    int getNewWriterId();
    int getNewBookId();

   public:
     XmlStorage(const string & dir_name) : dir_name_(dir_name) { }
     ~XmlStorage() override {}

     bool load() override;
     bool save() override;

     // writers
     vector<Writer> getAllWriters(void) override;
     optional<Writer> getWriterById(int writer_id) override;
     bool updateWriter(const Writer &writer) override;
     bool removeWriter(int writer_id) override;
     int insertWriter(const Writer &writer) override;

     //books
     vector<Book> getAllBooks(void) override;
     optional<Book> getBookById(int book_id) override;
     bool updateBook(const Book &book) override;
     bool removeBook(int book_id) override;
     int insertBook(const Book &book) override;
};
