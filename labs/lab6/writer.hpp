#pragma once

#include <vector>
#include <string>

using std::vector;
using std::string;  

class Writer
{
private:
    int Id;
    string Name;
    string Nation;
    int Born;
    int Died;
public:

    bool operator == (const Writer& wr) const;
    void operator = (const Writer& wr);

    void setId(int id);
    void setName(string name);
    void setNation(string nation);
    void setBorn(int born);
    void setDied(int died);

    int getId() const;
    string getName() const;
    string getNation() const;
    int getBorn() const;
    int getDied() const;
    
public:
    Writer() : Id(-1), Name(""), Nation(""), Born(-1), Died(-1) {}
    Writer(int id, string name, string nation, int born, int died) : Id(id), Name(name), Nation(nation), Born(born), Died(died) {}
    ~Writer() {}
};


