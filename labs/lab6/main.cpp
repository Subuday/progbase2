#include <QCoreApplication>
#include "cui.hpp"
#include "xml_storage.hpp"
#include "csv_storage.hpp"

int main(int argc, char *argv[])
{

    QCoreApplication a(argc, argv);
//    CsvStorage csv_storage("/data");
//    Storage * storage_ptr = &csv_storage;
//    storage_ptr->load();

    XmlStorage xml_storage("/data");
    Storage * storage_ptr = &xml_storage;
    storage_ptr->load();

    Cui cui(storage_ptr);
    cui.show();

    return a.exec();
}
