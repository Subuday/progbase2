#include "list.h"

void List_init(List* self) {
    self->capacity = 4;
    self->items = malloc(sizeof(void*) * self->capacity);
    if(self->items == NULL) {fprintf(stderr, "MEMORY ERROR"), abort();}
    self->size = 0;
}

void List_deinit(List* self) {
    free(self->items);
} 

List* List_alloc() {
    List* self = malloc(sizeof(struct List));
    if(self == NULL) {fprintf(stderr, "MEMORY ERROR"), abort();}
    self->capacity = 4;
    self->items = malloc(sizeof(void*) * self->capacity);
    if(self->items == NULL) {fprintf(stderr, "MEMORY ERROR"), abort();}
    self->size = 0;
    return self;
}

List* List_realloc(List* self) {
    if(self->capacity == self->size) {
        int newCapacity = 4 * self->capacity;
        void** item = realloc(self->items ,sizeof(void*) * newCapacity);
        if(item == NULL) {fprintf(stderr, "MEMORY ERROR"), abort();}
        self->capacity = newCapacity;
        self->items = item;
    }
    return self;
}

void List_add(List* self, void* item) {
    if(self->capacity == self->size) {
        int newCapacity = 4 * self->capacity;
        void** item = realloc(self->items ,sizeof(void*) * newCapacity);
        if(item == NULL) {fprintf(stderr, "MEMORY ERROR"), abort();}
        self->capacity = newCapacity;
        self->items = item;
    }
    self->items[self->size] = item;
    self->size++;
}

int List_size(List* self) {
    if(self != NULL) return self->size;
    return -1;
}


void* List_getIndexOf(List* self, int index) {
    if(index < self->size) return self->items[index];
    return NULL;
}

void List_clear(List* self) {
    for(size_t j = 0; j < self->size; ++j) {
            free(self->items[j]);
    }
}

void List_free(List* self) {
    free(self->items);
    free(self); 
}