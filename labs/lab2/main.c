// Компілювати за допомогою:
// gcc main.c -lprogbase -lm
// valgrind --leak-check=yes  ./lab ../data.csv -n 1900 -o ../test.csv
#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <stdbool.h>
#include <time.h>
#include <ctype.h> 
#include <string.h>
#include <assert.h>
#include <progbase.h> // Спрощений ввід і вивід даних у консоль
#include <unistd.h>  
#include <getopt.h>
#include <stdbool.h>
#include <unistd.h>  
#include <getopt.h>

#include "vec.h"
#include "list.h"
#include "csv.h"

struct Writer {
    char* Name;
    char* Nation;
    char* BithDate;
    char* Death;
    char* Book;
};

void testList(int N, char* outFile);

int main(int argc, char *argv[]) {

    int opt;  
    int N = -1;
    char* inFile = NULL;
    char* outFile = NULL;
    while((opt = getopt(argc, argv, "n:o:")) != -1)  
    {  
        switch(opt)  
        {  
            case 'n': 
                N = atoi(optarg);
                if(N < 0) return EXIT_FAILURE; 
                break;
            case 'o':  
                outFile = optarg; 
                break;  
            case ':':   
                fprintf(stderr, "option needs a value\n"); 
                return EXIT_FAILURE;
                break;  
            case '?':   
                fprintf(stderr, "unknown option: \n"); 
                return EXIT_FAILURE;
                break;  
        }  
    }  
      
    // optind is for the extra arguments 
    // which are not parsed 
    // for(; optind < argc; optind++){      
    //     printf("extra arguments: %s\n", argv[optind]);  
    // }
    if(optind < argc) {
        inFile = argv[optind];
    }

    // Початок програми
    if(inFile != NULL) {
        FILE *fp;
        fp = fopen(inFile, "r");
        if(fp == NULL) {fprintf(stderr, "FILE ISN'T OPENED"); return EXIT_FAILURE;};

        char ch = '\0';
        Vec* vec = Vec_init(10);

        while((ch = fgetc(fp)) != EOF) {
            Vec_add(vec, ch); 
        }
        Vec_add(vec, '\0');

        char* str = Vec_toStr(vec);
        char* header = Csv_getHeaderTableFromString(str);

        List table;
        List_init(&table);

        Csv_fillTableFromString(&table, str);
        Csv_printTable(&table);
        puts("");

        char* test = Csv_createStringFromTable(&table, N); 
        char* csvTabelStr = Csv_addHeaderToString(header, test);

        if(outFile != NULL) {
            FILE *fout;
            fout = fopen(outFile, "w");
            if(fout == NULL) {fprintf(stderr, "FILE ISN'T OPENED"); abort();};
            fprintf(fout, "%s", csvTabelStr);
            fclose(fout);
        } else {
            puts(csvTabelStr);
        }

        free(csvTabelStr);
        Csv_clearTable(&table);
        List_deinit(&table);
        Vec_deinit(vec); 
        fclose(fp);
    } else {
        testList(N, outFile);
    }




    // Кінець програми
    return 0;
}

void testList(int N, char* outFile) {

    struct Writer writers[] = {   
            {"Taras Shevchenko",     "Ukrainian",    "1814", "1861", "Kobzar"},
            {"Ernest Hemingway",     "American",     "1899", "1961", "\"The Old, Man and the Sea\""},
            {"Arthur Conan Doyle",   "Scottish",     "1899", "1930", "\"Stories of Sherlock Holmes\""},
            {"Heinrich Heine",       "German",       "1859", "1856", "Book of Songs"},
            {"Dante Alighieri",      "Italian",      "1265", "1321", "The divine comedy"}
        };

        int stLength = sizeof(writers) / sizeof(writers[0]);
        List table; 
        List_init(&table);

        for(int i = 0; i < stLength; ++i) {
            struct Writer *pWt = &writers[i];

            List* pRow = List_alloc();
            Csv_addString(pRow, pWt->Name);
            Csv_addString(pRow, pWt->Nation);
            Csv_addString(pRow, pWt->BithDate);
            Csv_addString(pRow, pWt->Death);
            Csv_addString(pRow, pWt->Book);

            Csv_addRow(&table, pRow);
        }
        Csv_printTable(&table);
        puts("");

        char* csvString = Csv_createStringFromTable(&table, N);
        
        if(outFile != NULL) {
            FILE *fout;
                fout = fopen(outFile, "w");
                if(fout == NULL) {fprintf(stderr, "FILE ISN'T OPENED"); abort();};
                fprintf(fout, "%s", csvString);
                fclose(fout);
        } else {
            puts(csvString);
        }

        free(csvString);
        Csv_clearTable(&table);
        List_deinit(&table);
}
