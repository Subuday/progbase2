// Компілювати за допомогою:
// gcc main.c -lprogbase -lm
#define _GNU_SOURCE
#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <stdbool.h>
#include <time.h>
#include <ctype.h> 
#include <string.h>
#include <assert.h>
#include <progbase.h> // Спрощений ввід і вивід даних у консоль

#include "list.h"
#include "queue.h"

void ListToQueue(const List* list, Queue* q, Queue* q2);
void QueuesToList(List* list, Queue* q, Queue* q2);

int main() {
    // Початок програми
    FILE *fp = NULL;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    
    fp = fopen("data.txt", "r");
    if(fp == NULL) {fprintf(stderr, "FILE OPENING ERROR"); return EXIT_FAILURE;}
     
    List* list = List_init(4); 
    Queue* q = Queue_init(15);
    Queue* q2 = Queue_init(3);
    
    while((read = getline(&line, &len, fp)) != -1) { 
        if(strlen(line) == 0 || (strlen(line) == 1 && line[0] == '\n')) continue;
        List_add(list, line);  
    }

    List_print(list);
    List_sort(list);   
    puts("----------------------TASK 1-------------------------------------------"); 
    List_print(list);

    puts("-----------------------TASK 2---------------------------------------------");
    ListToQueue(list, q, q2);
    Queue_print(q);
    puts("/////////////////////////////////////////////////////////////////////////");
    Queue_print(q2);

    puts("------------------------TASK 3-------------------------------------------");
    List* list2 = List_init(4); 
    QueuesToList(list2, q, q2);
    List_print(list2); 
 
    Queue_deinit(q); 
    Queue_deinit(q2);
    List_deinit(list);    
    List_deinit(list2);   
    if(line != NULL) free(line);    
    fclose(fp);     
    // Кінець програми 
    return 0;
}

void ListToQueue(const List* list, Queue* q, Queue* q2) {
    int listSize = List_size(list);
    for(int i = 0; i < listSize; ++i) {
        char* temp = List_get(list, i);
        if(i % 2 == 0) Queue_push(q2, temp); 
            else Queue_push(q, temp);  
    }
}

void QueuesToList(List* list, Queue* q, Queue* q2) {
    while(!Queue_isEmpty(q)) {
        char* str = Queue_pop(q);
        List_add(list, str);
        free(str);
    }

    while(!Queue_isEmpty(q2)) {
        char* str = Queue_pop(q2);
        List_add(list, str);
        free(str);
    }
}