#pragma once

#include <stdlib.h>
#include <stdio.h> 
#include <string.h>

typedef struct List List;

List* List_init(const int length);
void List_deinit(List* self);
void List_add(List* self, char* str);
void List_sort(List* self);
const void List_print(List* self);
size_t List_size(const List* self);
char* List_get(const List* self, const size_t index);