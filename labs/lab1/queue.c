#include "queue.h"

struct Queue {
    char **items;
    int capacity;
    int first;
    int last;
};

Queue* Queue_init(const int length) {
    if(length <= 0) {fprintf(stderr, "Memory can't be allocated ");  abort();}
    Queue* self = malloc(sizeof(struct Queue));
    self->capacity = 4 * length;
    self->items = malloc(sizeof(char*) * self->capacity);
    if(self->items == NULL) {fprintf(stderr, "Memory can't be allocated ");  abort();}
    self->first = 0;
    self->last = -1;

    return self;
}

void Queue_deinit(Queue* self) {
    for(int i = self->first; i <= self->last; ++i) {
        free(self->items[i]);
    }
    free(self->items);
    free(self);
}

void Queue_push(Queue* self, char* str) {
    int strLen = strlen(str);
    if(str[strLen - 1] == '\n') {str[strLen - 1] = '\0'; strLen--;}

    char* strItem = malloc(sizeof(char) * (strLen + 1));
    if(strItem == NULL) {fprintf(stderr, "Memory can't be allocated ");  abort();}
 
    strncpy(strItem, str, strlen(str) + 1);

    if(self->last + 1 == self->capacity) {
        if(self->first != 0) {
            for(int i = 0; i < self->first; ++i) {
                for(int j = 0; j < self->last; ++j) {
                    self->items[j] = self->items[j+1];
                }
            }
            self->first = 0;
            self->last = self->last - self->first;
        } else {
            int newCapacity = self->capacity * 4;
            char** newItems = realloc(self->items, sizeof(char*) * newCapacity);
            if(newItems == NULL) {
                Queue_deinit(self);
                fprintf(stderr, "Memory can't be allocated ");
                abort();
            }
            self->capacity = newCapacity;
            self->items = newItems;
        }
    }
    self->last++; 
    self->items[self->last] = strItem;   
}

const void Queue_print(Queue *self) {
    for(int i = self->first; i <= self->last; ++i) {
        printf("%s", self->items[i]);
        putchar('\n');
    }
}

int Queue_size(const Queue* self) {
    return (self->last - self->first + 1);
}

void Queue_clear(Queue *self) {
    for(int i = self->first; i <= self->last; ++i) {
        free(self->items[i]);
        self->items[i] = NULL;
    }
    self->first = 0;
    self->last = -1;
}

bool Queue_isEmpty(const Queue *self) {
    int size = Queue_size(self);
    if(size == 0) return true;
    return false;
}

char* Queue_pop(Queue *self) {
    if(self->items[self->first] != NULL) {
        char* str = self->items[self->first];
        self->items[self->first] = NULL;
        self->first++;
        return str;
    } else {
        return NULL;
    }
}
