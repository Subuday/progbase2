#include "list.h"

struct List {
    char** items;
    size_t len;
    size_t capacity;
};


List* List_init(const int length) {
    if(length <= 0) {fprintf(stderr, "Memory can't be allocated ");  abort();}
    List* self = malloc(sizeof(struct List));
    if(self == NULL) {fprintf(stderr, "Memory can't be allocated ");  abort();}
    self->capacity = 4 * length;
    self->items = malloc(sizeof(char*) * self->capacity);
    if(self->items == NULL) {fprintf(stderr, "Memory can't be allocated ");  abort();}
    self->len = 0;
    return self;
}

void List_deinit(List* self) {
    for(size_t i = 0; i < self->len; ++i) free(self->items[i]);
    free(self->items);
    free(self);
}

void List_add(List* self, char* str) {
    if(self->capacity == self->len) {
        size_t newCapacity = self->capacity * 4;
        char** newItems = realloc(self->items ,sizeof(char*) * newCapacity); 
        if(newItems == NULL) {
            List_deinit(self);
            fprintf(stderr, "Memory can't be allocated ");
            abort();
        }
        self->capacity = newCapacity;
        self->items = newItems;
    }
    int strLen = strlen(str);
    if(str[strLen - 1] == '\n') {str[strLen - 1] = '\0'; strLen--;}

    char* strItem = malloc(sizeof(char) * (strLen + 1)); 
    if(strItem == NULL) {fprintf(stderr, "Memory can't be allocated ");  abort();}
 
    strncpy(strItem, str, strlen(str) + 1); 
    self->items[self->len] = strItem;
    self->len++;
}

void List_sort(List* self) {
    size_t i = 0;
    for(i = 0; i < self->len; ++i) {
        size_t strLen = strlen(self->items[i]);
        if(strLen < 10) {
            char* temp = self->items[i];
            for(size_t j = i; j >= 1; --j) {
                self->items[j] = self->items[j-1];
            }
            self->items[0] = temp;
        }
    }
}

const void List_print(List *self) {
    for(size_t i = 0; i < self->len; ++i) {
            printf("%s", self->items[i]);
            putchar('\n');
    }
}

size_t List_size(const List* self) {
    return self->len;
}

char* List_get(const List* self, const size_t index) {
    size_t size = List_size(self);
    if(index <= size - 1) {
        return self->items[index];
    }
    return NULL;
}