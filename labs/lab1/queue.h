#pragma once

#include <stdlib.h>
#include <stdio.h> 
#include <string.h>
#include <stdbool.h>

typedef struct Queue Queue;

Queue* Queue_init(const int length);
void Queue_deinit(Queue* self);
void Queue_push(Queue* self, char* str);
const void Queue_print(Queue *self);
int Queue_size(const Queue *self);
void Queue_clear(Queue *self);
bool Queue_isEmpty(const Queue *self);
char* Queue_pop(Queue *self);