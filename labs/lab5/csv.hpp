#pragma once

#include <vector>
#include <string>

using std::vector;
using std::string;  

using CsvRow = vector<string>;  
using CsvTable = vector<CsvRow>; 

namespace Csv
{
// parse csv, create & fill string table
CsvTable createTableFromString(const string &csv_str); 
// return a string filled with csv from string table
string createStringFromTable(const CsvTable &csv_table); 
}