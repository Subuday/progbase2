#pragma once

#include "storage.hpp"

class Cui
{
    Storage * const storage_;

    void mainMenu();

    // writers menus
    void writersMainMenu();
    void writersMenu();
    void writerMenu(int entity_id);
    void writerUpdateMenu(int entity_id);
    void writerDeleteMenu(int entity_id);
    void writerCreateMenu();

    //books menus
    void booksMainMenu();
    void booksMenu();
    void bookMenu(int entity_id);
    void bookUpdateMenu(int entity_id);
    void bookDeleteMenu(int entity_id);
    void bookCreateMenu();
    
public:
    Cui(Storage * storage): storage_(storage) {}
    //
    void show();
};