#include "storage.hpp"

bool Storage::load()
{
    ifstream file;
    file.exceptions (ifstream::failbit | ifstream::badbit);

    string csv_str;
    string csv_str_extra;

    try {
        file.open(dir_name_ + "/writers.csv");
        string line;
        while(!file.eof()) {
            getline(file, line);
            csv_str += line;
            csv_str += "\n";
        } 
        file.close();
    } catch(const ios_base::failure & e) {
        cerr << "Execption  opening/reading/closing file " << flush;
        cerr << e.what() << flush;
        return false;
    }

    try {
        file.open(dir_name_ + "/books.csv");
        string line;
        while(!file.eof()) {
            getline(file, line);
            csv_str_extra += line;
            csv_str_extra += "\n";
        } 
        file.close();
    } catch(const ios_base::failure & e) {
        cerr << "Execption  opening/reading/closing file " << flush;
        cerr << e.what() << flush;
        return false;
    }

    CsvTable csv_table = Csv::createTableFromString(csv_str);
    CsvTable csv_table_extra = Csv::createTableFromString(csv_str_extra);

    try
    {   
        bool skip = true;
        for(auto it : csv_table) 
        {
            if(skip) { skip = false; continue;}
            Writer writer = rowToWriter(it);
            writers_.push_back(writer);
        }

        skip = true;
        for(auto it : csv_table_extra)
        {
            if(skip) { skip = false; continue;}
            Book book = rowToBook(it);
            books_.push_back(book);
        }
    }
    catch(const std::invalid_argument& ia)
    {
        cerr << ia.what() << flush;
        return false;
    }

    return true;
}

Writer Storage::rowToWriter(const CsvRow &row)
{
    Writer writer;

    try 
    {
        writer.setId(stoi(row.at(0)));
        writer.setName(row.at(1));
        writer.setNation(row.at(2));
        writer.setBorn(stoi(row.at(3)));
        writer.setDied(stoi(row.at(4)));
    } 
    catch (const std::invalid_argument& ia)
    {
        std::cerr << "Invalid argument: " << ia.what() << std::endl;
        throw ia;
    }
    
    return writer;
}

bool Storage::save()
{
    ofstream file;

    try
    {
        file.open(dir_name_ + "/writers.csv");
        CsvTable csv_table;

        for(auto it : writers_)
        {
            CsvRow row = writerToRow(it);
            csv_table.push_back(row);
        }

        string csv_str = Csv::createStringFromTable(csv_table);
        file << csv_str;
        file.close();
    }
    catch(ofstream::failure &e) 
    {
        cerr << "Execption  opening/writing/closing file " << flush;
        cerr << e.what() << flush;
        return false;
    }

    try
    {
        file.open(dir_name_ + "/books.csv");
        CsvTable csv_table_extra;

        for(auto it : books_)
        {
            CsvRow row = bookToRow(it);
            csv_table_extra.push_back(row);
        }

        string csv_str = Csv::createStringFromTable(csv_table_extra);
        file << csv_str;
        file.close();
    }
    catch(ofstream::failure &e) 
    {
        cerr << "Execption  opening/writing/closing file " << flush;
        cerr << e.what() << flush;
        return false;
    }

    return true;
}

CsvRow Storage::writerToRow(const Writer &wr)
{
    CsvRow row;
    row.push_back(to_string(wr.getId()));
    row.push_back(wr.getName());
    row.push_back(wr.getNation());
    row.push_back(to_string(wr.getBorn()));
    row.push_back(to_string(wr.getDied()));

    return row;
}

vector<Writer> Storage::getAllWriters(void)
{
    return writers_;
}

optional<Writer> Storage::getWriterById(int writer_id)
{
    for(auto & it : writers_)
    {
        if(it.getId() == writer_id)
        {
            return it;
        }
    }

    return nullopt;
}

bool Storage::updateWriter(const Writer &writer)
{
    for(auto &it : writers_)
    {
        if(it == writer)
        {
            it = writer;
            return true;
        }
    }
    return false;
}

bool Storage::removeWriter(int writer_id)
{
    int i = 0;
    for(auto &it : writers_)
    {
        if(it.getId() == writer_id)
        {
            writers_.erase(writers_.begin() + i);
            return true;
        }
        ++i;
    }
    return false;
}

int Storage::insertWriter(const Writer &writer)
{
    int id = getNewWriterId();
    Writer wrWithNewId = writer;
    wrWithNewId.setId(id);
    writers_.push_back(wrWithNewId);
    return id;
}

int Storage::getNewWriterId()
{
    if(!writers_.empty())
    {
        Writer writer = writers_.back();
        int id = writer.getId();
        return ++id;
    }
    return 1;
}

Book Storage::rowToBook(const CsvRow &row)
{
    Book book;

    try 
    {
        book.setId(stoi(row.at(0)));
        book.setName(row.at(1));
        book.setGenre(row.at(2));
        book.setYear(stoi(row.at(3)));
    } 
    catch (const std::invalid_argument& ia)
    {
        std::cerr << "Invalid argument: " << ia.what() << std::endl;
        throw ia;
    }
    
    return book;
}

CsvRow Storage::bookToRow(const Book &bk)
{
    CsvRow row;
    row.push_back(to_string(bk.getId()));
    row.push_back(bk.getName());
    row.push_back(bk.getGenre());
    row.push_back(to_string(bk.getYear()));

    return row;
}

vector<Book> Storage::getAllBooks(void)
{
    return books_;
}

optional<Book> Storage::getBookById(int book_id)
{
    for(auto & it : books_)
    {
        if(it.getId() == book_id)
        {
            return it;
        }
    }

    return nullopt;
}

bool Storage::updateBook(const Book &book)
{
    for(auto &it : books_)
    {
        if(it == book)
        {
            it = book;
            return true;
        }
    }
    return false;
}

bool Storage::removeBook(int book_id)
{
    int i = 0;
    for(auto &it : books_)
    {
        if(it.getId() == book_id)
        {
            books_.erase(books_.begin() + i);
            return true;
        }
        ++i;
    }
    return false;
}

int Storage::insertBook(const Book &book)
{
    int id = getNewBookId();
    Book bkWithNewId = book;
    bkWithNewId.setId(id);
    books_.push_back(bkWithNewId);
    return id;
}

int Storage::getNewBookId()
{
    if(!books_.empty())
    {
        Book book = books_.back();
        int id = book.getId();
        return ++id;
    }
    return 1;
}