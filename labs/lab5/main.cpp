// Компілювати за допомогою:
// g++ main.cpp -g

#include <iostream>
#include "storage.hpp"
#include "cui.hpp"

using namespace std;

int main() {
    // Start
    Storage storage("../data");
    storage.load();

    Cui cui(&storage);
    cui.show();
 
    // End
    return 0;
}