#pragma once

#include <vector>
#include <string>

using std::vector;
using std::string;  

class Book
{
private:
    int Id;
    string Name;
    string Genre;
    int Year;
public:

    bool operator == (const Book& bk) const;
    void operator = (const Book& bk);

    void setId(int id);
    void setName(string name);
    void setGenre(string genre);
    void setYear(int year);

    int getId() const;
    string getName() const;
    string getGenre() const;
    int getYear() const;
    
public:
    Book() : Id(-1), Name(""), Genre(""), Year(-1) {};
    Book(int id, string name, string genre, int year) : Id(id), Name(name), Genre(genre), Year(year) {};
    ~Book() {};
};