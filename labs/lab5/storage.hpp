#pragma once

#include <iostream>  
#include <cstdlib> 
#include <fstream>
#include <vector>
#include <string>

#include "optional.hpp"
#include "writer.hpp"
#include "book.hpp"
#include "csv.hpp"

using namespace std;

class Storage
{
  const string dir_name_;

  vector<Writer> writers_;
  vector<Book> books_;

  static Writer rowToWriter(const CsvRow &row);
  static CsvRow writerToRow(const Writer &wr);
  static Book rowToBook(const CsvRow &row);
  static CsvRow bookToRow(const Book &bk);

  int getNewWriterId();
  int getNewBookId();

 public:
   Storage(const string & dir_name) : dir_name_(dir_name) { }
   bool load();
   bool save();
   // writers
   vector<Writer> getAllWriters(void);
   optional<Writer> getWriterById(int writer_id);
   bool updateWriter(const Writer &writer);
   bool removeWriter(int writer_id);
   int insertWriter(const Writer &writer);
   //books
   vector<Book> getAllBooks(void);
   optional<Book> getBookById(int book_id);
   bool updateBook(const Book &book);
   bool removeBook(int book_id);
   int insertBook(const Book &book);
};
