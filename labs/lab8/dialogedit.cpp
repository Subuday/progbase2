#include "dialogedit.h"
#include "ui_dialogedit.h"

#include <iostream>

DialogEdit::DialogEdit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogEdit)
{
    ui->setupUi(this);

    validator = new QIntValidator(0,9999,this);

    lineEditName = findChild<QLineEdit*>("lineEditName");
    lineEditNation = findChild<QLineEdit*>("lineEditNation");
    lineEditBorn = findChild<QLineEdit*>("lineEditBorn");
    lineEditBorn->setValidator(validator);
    lineEditDead = findChild<QLineEdit*>("lineEditDead");
    lineEditDead->setValidator(validator);

    connect(this, SIGNAL(accepted()), this, SLOT(updateWriter()));
}

DialogEdit::~DialogEdit()
{
    delete ui;
}

void DialogEdit::setEditName(QString name)
{
    lineEditName->setText(name);
}

void DialogEdit::setEditNation(QString nation)
{
    lineEditNation->setText(nation);
}

void DialogEdit::setEditBorn(QString born)
{
    lineEditBorn->setText(born);
}

void DialogEdit::setEditDead(QString dead)
{
    lineEditDead->setText(dead);
}

Writer DialogEdit::getWriter()
{
    return writer;
}

void DialogEdit::updateWriter()
{
    writer.setName(lineEditName->text());
    writer.setNation(lineEditNation->text());
    writer.setBorn(lineEditBorn->text().toInt());
    writer.setDied(lineEditDead->text().toInt());
}
