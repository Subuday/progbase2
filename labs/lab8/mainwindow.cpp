#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "iostream"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    listWidget = this->findChild<QListWidget*>("listWidget");
    addButton = this->findChild<QPushButton*>("addButton");
    editButton = this->findChild<QPushButton*>("editButton");
    deleteButton = this->findChild<QPushButton*>("deleteButton");

    labelSelectObj = this->findChild<QLabel*>("labelSelectObj");
    labelID = this->findChild<QLabel*>("labelID");
    labelName = this->findChild<QLabel*>("labelName");
    labelNation = this->findChild<QLabel*>("labelNation");
    labelBorn = this->findChild<QLabel*>("labelBorn");
    labelDead = this->findChild<QLabel*>("labelDead");
    labelWriterID = this->findChild<QLabel*>("labelWriterID");
    labelWriterName = this->findChild<QLabel*>("labelWriterName");
    labelWriterNation = this->findChild<QLabel*>("labelWriterNation");
    labelWriterBorn = this->findChild<QLabel*>("labelWriterBorn");
    labelWriterDead = this->findChild<QLabel*>("labelWriterDead");

    labelsSetEnabledState(false);

    connect(ui->listWidget, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(onListWritersItemClicked(QListWidgetItem*)));

    //storage = new XmlStorage("/data");
    storage = new SQLiteStorage("/data");
    if(storage != nullptr && storage->load()) loadDataToListWidget();
    else QMessageBox::critical(nullptr, QObject::tr("Error"),
                               QObject::tr("Out of memory."), QMessageBox::Cancel);
}

MainWindow::~MainWindow()
{
    listWidget->clear();
    delete storage;
    delete ui;
}

void MainWindow::on_addButton_clicked()
{
    DialogAdd dialog;
    if(dialog.exec() == QDialog::Accepted)
    {
        Writer writer = dialog.getWriter();
        int id = storage->insertWriter(writer);
        writer.setId(id);

        QVariant qVariant;
        qVariant.setValue(writer);

        QListWidgetItem *qWriterListItem = new QListWidgetItem();
        qWriterListItem->setText(QString::number(writer.getBorn()) + " " + writer.getName());
        qWriterListItem->setData(Qt::UserRole, qVariant);

        listWidget->addItem(qWriterListItem);
    }
    editButton->setEnabled(false);
    deleteButton->setEnabled(false);
    labelsSetEnabledState(false);
}

void MainWindow::on_editButton_clicked()
{
    QListWidgetItem* item = listWidget->currentItem();

    QVariant qVariant = item->data(Qt::UserRole);
    Writer writer = qVariant.value<Writer>();
    int id = writer.getId();

    DialogEdit dialog;
    dialog.setWindowTitle("Edit");
    dialog.setEditName(writer.getName());
    dialog.setEditNation(writer.getNation());
    dialog.setEditBorn(QString::number(writer.getBorn()));
    dialog.setEditDead(QString::number(writer.getDied()));

    if(dialog.exec() == QDialog::Accepted)
    {
        writer = dialog.getWriter();
        writer.setId(id);
        storage->updateWriter(writer);

        qVariant.setValue(writer);

        item->setText(QString::number(writer.getBorn()) + " " + writer.getName());
        item->setData(Qt::UserRole, qVariant);
    }
    editButton->setEnabled(false);
    deleteButton->setEnabled(false);
    labelsSetEnabledState(false);
}

void MainWindow::on_deleteButton_clicked()
{
    QListWidgetItem* item = listWidget->currentItem();

    QVariant qVariant = item->data(Qt::UserRole);
    Writer writer = qVariant.value<Writer>();

    DialogDelete dialog;
    dialog.setWindowTitle("Delete");
    dialog.setLabelWrID(QString::number(writer.getId()));
    dialog.setLabelWrName(writer.getName());
    if(dialog.exec() == QDialog::Accepted)
    {
        storage->removeWriter(writer.getId());
        listWidget->removeItemWidget(item);
        delete item;
    }
    editButton->setEnabled(false);
    deleteButton->setEnabled(false);
    labelsSetEnabledState(false);
}

void MainWindow::onListWritersItemClicked(QListWidgetItem* item)
{
    QVariant qVariant = item->data(Qt::UserRole);
    Writer writer = qVariant.value<Writer>();

    labelWriterID->setNum(writer.getId());
    labelWriterName->setText(writer.getName());
    labelWriterNation->setText(writer.getNation());
    labelWriterBorn->setNum(writer.getBorn());
    labelWriterDead->setNum(writer.getDied());

    addButton->setEnabled(true);
    editButton->setEnabled(true);
    deleteButton->setEnabled(true);
    labelsSetEnabledState(true);
}

void MainWindow::loadDataToListWidget()
{
    for(auto it : storage->getAllWriters())
    {
        QVariant qVariant;
        qVariant.setValue(it);

        QListWidgetItem *qWriterListItem = new QListWidgetItem();
        qWriterListItem->setText(QString::number(it.getBorn()) + " " + it.getName());
        qWriterListItem->setData(Qt::UserRole, qVariant);

        ui->listWidget->addItem(qWriterListItem);
    }
}

void MainWindow::labelsSetEnabledState(bool state)
{
    labelSelectObj->setVisible(state);
    labelID->setVisible(state);
    labelName->setVisible(state);
    labelNation->setVisible(state);
    labelBorn->setVisible(state);
    labelDead->setVisible(state);
    labelWriterID->setVisible(state);
    labelWriterName->setVisible(state);
    labelWriterNation->setVisible(state);
    labelWriterBorn->setVisible(state);
    labelWriterDead->setVisible(state);
}
