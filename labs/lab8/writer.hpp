#pragma once

#include <QMetaType>
#include <QString>

#include <vector>
#include <string>

using std::vector;
using std::string;  

class Writer
{
private:
    int Id;
    QString Name;
    QString Nation;
    int Born;
    int Died;
public:

    bool operator == (const Writer& wr) const;
    void operator = (const Writer& wr);

    void setId(int id);
    void setName(QString name);
    void setNation(QString nation);
    void setBorn(int born);
    void setDied(int died);

    int getId() const;
    QString getName() const;
    QString getNation() const;
    int getBorn() const;
    int getDied() const;
    
public:
    Writer() : Id(-1), Name(""), Nation(""), Born(-1), Died(-1) {}
    Writer(int id, QString name, QString nation, int born, int died) : Id(id), Name(name), Nation(nation), Born(born), Died(died) {}
    ~Writer() {}
};

Q_DECLARE_METATYPE(Writer)


