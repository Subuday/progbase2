#pragma once

#include <QString>

#include <vector>
#include <string>

using std::vector;
using std::string;  

class Book
{
private:
    int Id;
    QString Name;
    QString Genre;
    int Year;
public:

    bool operator == (const Book& bk) const;
    void operator = (const Book& bk);

    void setId(int id);
    void setName(QString name);
    void setGenre(QString genre);
    void setYear(int year);

    int getId() const;
    QString getName() const;
    QString getGenre() const;
    int getYear() const;
    
public:
    Book() : Id(-1), Name(""), Genre(""), Year(-1) {}
    Book(int id, QString name, QString genre, int year) : Id(id), Name(name), Genre(genre), Year(year) {}
    ~Book() {}
};
