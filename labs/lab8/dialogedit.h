#ifndef DIALOGEDIT_H
#define DIALOGEDIT_H

#include <QDialog>
#include <QLineEdit>

#include "writer.hpp"

namespace Ui {
class DialogEdit;
}

class DialogEdit : public QDialog
{
    Q_OBJECT

public:
    explicit DialogEdit(QWidget *parent = nullptr);
    ~DialogEdit();

    void setEditName(QString name);
    void setEditNation(QString nation);
    void setEditBorn(QString born);
    void setEditDead(QString dead);

    Writer getWriter();

private slots:
    void updateWriter();

private:
    Ui::DialogEdit *ui;

    Writer writer;

    QValidator* validator;

    QLineEdit* lineEditName;
    QLineEdit* lineEditNation;
    QLineEdit* lineEditBorn;
    QLineEdit* lineEditDead;

};

#endif // DIALOGEDIT_H
