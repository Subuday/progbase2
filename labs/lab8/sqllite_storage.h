#pragma once

#include <QtSql>
#include <QMessageBox>

#include "storage.hpp"

class SQLiteStorage : Storage
{
    const string dir_name_;

    QSqlDatabase db;

public:
    SQLiteStorage() {}
    SQLiteStorage(const string & dir_name) : dir_name_(dir_name) { }
    ~SQLiteStorage() override {if(db.open()) db.close();}

    bool load() override;
    bool save() override;

    // writers
    vector<Writer> getAllWriters(void) override;
    optional<Writer> getWriterById(int writer_id) override;
    bool updateWriter(const Writer &writer) override;
    bool removeWriter(int writer_id) override;
    int insertWriter(const Writer &writer) override;

    //books
    vector<Book> getAllBooks(void) override;
    optional<Book> getBookById(int book_id) override;
    bool updateBook(const Book &book) override;
    bool removeBook(int book_id) override;
    int insertBook(const Book &book) override;
};

