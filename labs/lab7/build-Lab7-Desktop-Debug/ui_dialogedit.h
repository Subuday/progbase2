/********************************************************************************
** Form generated from reading UI file 'dialogedit.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGEDIT_H
#define UI_DIALOGEDIT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_DialogEdit
{
public:
    QDialogButtonBox *buttonBox;
    QLabel *labelMain;
    QLabel *labelName;
    QLabel *labelNation;
    QLabel *labelBorn;
    QLabel *labelDead;
    QLineEdit *lineEditName;
    QLineEdit *lineEditNation;
    QLineEdit *lineEditBorn;
    QLineEdit *lineEditDead;
    QPushButton *pushButtonAdd;
    QPushButton *pushButtonRemove;
    QListWidget *listExtra;
    QListWidget *listUserData;

    void setupUi(QDialog *DialogEdit)
    {
        if (DialogEdit->objectName().isEmpty())
            DialogEdit->setObjectName(QStringLiteral("DialogEdit"));
        DialogEdit->resize(690, 474);
        buttonBox = new QDialogButtonBox(DialogEdit);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(320, 430, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        labelMain = new QLabel(DialogEdit);
        labelMain->setObjectName(QStringLiteral("labelMain"));
        labelMain->setGeometry(QRect(160, 10, 361, 21));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        labelMain->setFont(font);
        labelName = new QLabel(DialogEdit);
        labelName->setObjectName(QStringLiteral("labelName"));
        labelName->setGeometry(QRect(160, 50, 71, 21));
        labelName->setFont(font);
        labelNation = new QLabel(DialogEdit);
        labelNation->setObjectName(QStringLiteral("labelNation"));
        labelNation->setGeometry(QRect(160, 90, 101, 21));
        labelNation->setFont(font);
        labelBorn = new QLabel(DialogEdit);
        labelBorn->setObjectName(QStringLiteral("labelBorn"));
        labelBorn->setGeometry(QRect(160, 130, 71, 21));
        labelBorn->setFont(font);
        labelDead = new QLabel(DialogEdit);
        labelDead->setObjectName(QStringLiteral("labelDead"));
        labelDead->setGeometry(QRect(160, 170, 71, 21));
        labelDead->setFont(font);
        lineEditName = new QLineEdit(DialogEdit);
        lineEditName->setObjectName(QStringLiteral("lineEditName"));
        lineEditName->setGeometry(QRect(270, 50, 281, 25));
        lineEditNation = new QLineEdit(DialogEdit);
        lineEditNation->setObjectName(QStringLiteral("lineEditNation"));
        lineEditNation->setGeometry(QRect(270, 90, 281, 25));
        lineEditBorn = new QLineEdit(DialogEdit);
        lineEditBorn->setObjectName(QStringLiteral("lineEditBorn"));
        lineEditBorn->setGeometry(QRect(270, 130, 81, 25));
        lineEditDead = new QLineEdit(DialogEdit);
        lineEditDead->setObjectName(QStringLiteral("lineEditDead"));
        lineEditDead->setGeometry(QRect(270, 170, 81, 25));
        pushButtonAdd = new QPushButton(DialogEdit);
        pushButtonAdd->setObjectName(QStringLiteral("pushButtonAdd"));
        pushButtonAdd->setEnabled(false);
        pushButtonAdd->setGeometry(QRect(298, 234, 101, 41));
        pushButtonRemove = new QPushButton(DialogEdit);
        pushButtonRemove->setObjectName(QStringLiteral("pushButtonRemove"));
        pushButtonRemove->setEnabled(false);
        pushButtonRemove->setGeometry(QRect(300, 320, 101, 41));
        listExtra = new QListWidget(DialogEdit);
        listExtra->setObjectName(QStringLiteral("listExtra"));
        listExtra->setGeometry(QRect(410, 210, 256, 192));
        listUserData = new QListWidget(DialogEdit);
        listUserData->setObjectName(QStringLiteral("listUserData"));
        listUserData->setGeometry(QRect(30, 210, 256, 192));

        retranslateUi(DialogEdit);
        QObject::connect(buttonBox, SIGNAL(accepted()), DialogEdit, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), DialogEdit, SLOT(reject()));

        QMetaObject::connectSlotsByName(DialogEdit);
    } // setupUi

    void retranslateUi(QDialog *DialogEdit)
    {
        DialogEdit->setWindowTitle(QApplication::translate("DialogEdit", "Dialog", Q_NULLPTR));
        labelMain->setText(QApplication::translate("DialogEdit", "Do you want to edit information about writer?", Q_NULLPTR));
        labelName->setText(QApplication::translate("DialogEdit", "Name:", Q_NULLPTR));
        labelNation->setText(QApplication::translate("DialogEdit", "Nationality:", Q_NULLPTR));
        labelBorn->setText(QApplication::translate("DialogEdit", "Born:", Q_NULLPTR));
        labelDead->setText(QApplication::translate("DialogEdit", "Dead:", Q_NULLPTR));
        pushButtonAdd->setText(QApplication::translate("DialogEdit", "Add", Q_NULLPTR));
        pushButtonRemove->setText(QApplication::translate("DialogEdit", "Remove", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DialogEdit: public Ui_DialogEdit {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGEDIT_H
