/********************************************************************************
** Form generated from reading UI file 'dialogdelete.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGDELETE_H
#define UI_DIALOGDELETE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>

QT_BEGIN_NAMESPACE

class Ui_DialogDelete
{
public:
    QDialogButtonBox *buttonBox;
    QLabel *labelName;
    QLabel *labelID;
    QLabel *labelWrName;
    QLabel *labelMain;
    QLabel *labelWrID;

    void setupUi(QDialog *DialogDelete)
    {
        if (DialogDelete->objectName().isEmpty())
            DialogDelete->setObjectName(QStringLiteral("DialogDelete"));
        DialogDelete->resize(337, 156);
        buttonBox = new QDialogButtonBox(DialogDelete);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(-20, 120, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        labelName = new QLabel(DialogDelete);
        labelName->setObjectName(QStringLiteral("labelName"));
        labelName->setGeometry(QRect(40, 40, 67, 17));
        labelID = new QLabel(DialogDelete);
        labelID->setObjectName(QStringLiteral("labelID"));
        labelID->setGeometry(QRect(40, 70, 67, 17));
        labelWrName = new QLabel(DialogDelete);
        labelWrName->setObjectName(QStringLiteral("labelWrName"));
        labelWrName->setGeometry(QRect(150, 70, 151, 17));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        labelWrName->setFont(font);
        labelMain = new QLabel(DialogDelete);
        labelMain->setObjectName(QStringLiteral("labelMain"));
        labelMain->setGeometry(QRect(40, 10, 291, 20));
        QFont font1;
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setWeight(75);
        labelMain->setFont(font1);
        labelWrID = new QLabel(DialogDelete);
        labelWrID->setObjectName(QStringLiteral("labelWrID"));
        labelWrID->setGeometry(QRect(150, 40, 101, 17));
        labelWrID->setFont(font);

        retranslateUi(DialogDelete);
        QObject::connect(buttonBox, SIGNAL(accepted()), DialogDelete, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), DialogDelete, SLOT(reject()));

        QMetaObject::connectSlotsByName(DialogDelete);
    } // setupUi

    void retranslateUi(QDialog *DialogDelete)
    {
        DialogDelete->setWindowTitle(QApplication::translate("DialogDelete", "Dialog", Q_NULLPTR));
        labelName->setText(QApplication::translate("DialogDelete", "ID:", Q_NULLPTR));
        labelID->setText(QApplication::translate("DialogDelete", "Name:", Q_NULLPTR));
        labelWrName->setText(QApplication::translate("DialogDelete", "TextLabel", Q_NULLPTR));
        labelMain->setText(QApplication::translate("DialogDelete", "Do you want to delete this writer?", Q_NULLPTR));
        labelWrID->setText(QApplication::translate("DialogDelete", "TextLabel", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DialogDelete: public Ui_DialogDelete {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGDELETE_H
