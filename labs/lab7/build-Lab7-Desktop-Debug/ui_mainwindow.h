/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QPushButton *addButton;
    QPushButton *editButton;
    QPushButton *deleteButton;
    QListWidget *listWidget;
    QLabel *labelSelectObj;
    QLabel *labelName;
    QLabel *labelNation;
    QLabel *labelBorn;
    QLabel *labelDead;
    QLabel *labelWriterID;
    QLabel *labelWriterName;
    QLabel *labelWriterBorn;
    QLabel *labelWriterDead;
    QLabel *labelID;
    QLabel *labelWriterNation;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(466, 418);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        addButton = new QPushButton(centralWidget);
        addButton->setObjectName(QStringLiteral("addButton"));
        addButton->setEnabled(true);
        addButton->setGeometry(QRect(310, 10, 91, 31));
        editButton = new QPushButton(centralWidget);
        editButton->setObjectName(QStringLiteral("editButton"));
        editButton->setEnabled(false);
        editButton->setGeometry(QRect(310, 60, 91, 51));
        deleteButton = new QPushButton(centralWidget);
        deleteButton->setObjectName(QStringLiteral("deleteButton"));
        deleteButton->setEnabled(false);
        deleteButton->setGeometry(QRect(310, 120, 91, 31));
        listWidget = new QListWidget(centralWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(0, 0, 301, 161));
        labelSelectObj = new QLabel(centralWidget);
        labelSelectObj->setObjectName(QStringLiteral("labelSelectObj"));
        labelSelectObj->setGeometry(QRect(10, 170, 141, 51));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        labelSelectObj->setFont(font);
        labelName = new QLabel(centralWidget);
        labelName->setObjectName(QStringLiteral("labelName"));
        labelName->setGeometry(QRect(10, 240, 81, 31));
        labelNation = new QLabel(centralWidget);
        labelNation->setObjectName(QStringLiteral("labelNation"));
        labelNation->setGeometry(QRect(10, 260, 101, 41));
        labelBorn = new QLabel(centralWidget);
        labelBorn->setObjectName(QStringLiteral("labelBorn"));
        labelBorn->setGeometry(QRect(10, 290, 81, 41));
        labelDead = new QLabel(centralWidget);
        labelDead->setObjectName(QStringLiteral("labelDead"));
        labelDead->setGeometry(QRect(10, 320, 91, 41));
        labelWriterID = new QLabel(centralWidget);
        labelWriterID->setObjectName(QStringLiteral("labelWriterID"));
        labelWriterID->setGeometry(QRect(100, 220, 121, 21));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        labelWriterID->setFont(font1);
        labelWriterID->setAutoFillBackground(false);
        labelWriterName = new QLabel(centralWidget);
        labelWriterName->setObjectName(QStringLiteral("labelWriterName"));
        labelWriterName->setGeometry(QRect(100, 240, 201, 31));
        labelWriterName->setFont(font1);
        labelWriterBorn = new QLabel(centralWidget);
        labelWriterBorn->setObjectName(QStringLiteral("labelWriterBorn"));
        labelWriterBorn->setGeometry(QRect(100, 290, 91, 41));
        labelWriterBorn->setFont(font1);
        labelWriterDead = new QLabel(centralWidget);
        labelWriterDead->setObjectName(QStringLiteral("labelWriterDead"));
        labelWriterDead->setGeometry(QRect(100, 330, 111, 21));
        labelWriterDead->setFont(font1);
        labelID = new QLabel(centralWidget);
        labelID->setObjectName(QStringLiteral("labelID"));
        labelID->setGeometry(QRect(10, 210, 81, 41));
        labelWriterNation = new QLabel(centralWidget);
        labelWriterNation->setObjectName(QStringLiteral("labelWriterNation"));
        labelWriterNation->setGeometry(QRect(100, 270, 201, 21));
        labelWriterNation->setFont(font1);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 466, 22));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        addButton->setText(QApplication::translate("MainWindow", "Add", Q_NULLPTR));
        editButton->setText(QApplication::translate("MainWindow", "Edit", Q_NULLPTR));
        deleteButton->setText(QApplication::translate("MainWindow", "Remove", Q_NULLPTR));
        labelSelectObj->setText(QApplication::translate("MainWindow", "Selected Object:", Q_NULLPTR));
        labelName->setText(QApplication::translate("MainWindow", "Name:", Q_NULLPTR));
        labelNation->setText(QApplication::translate("MainWindow", "Nationality:", Q_NULLPTR));
        labelBorn->setText(QApplication::translate("MainWindow", "Born:", Q_NULLPTR));
        labelDead->setText(QApplication::translate("MainWindow", "Dead:", Q_NULLPTR));
        labelWriterID->setText(QApplication::translate("MainWindow", "TextLabel", Q_NULLPTR));
        labelWriterName->setText(QApplication::translate("MainWindow", "TextLabel", Q_NULLPTR));
        labelWriterBorn->setText(QApplication::translate("MainWindow", "TextLabel", Q_NULLPTR));
        labelWriterDead->setText(QApplication::translate("MainWindow", "TextLabel", Q_NULLPTR));
        labelID->setText(QApplication::translate("MainWindow", "ID:", Q_NULLPTR));
        labelWriterNation->setText(QApplication::translate("MainWindow", "TextLabel", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
