/********************************************************************************
** Form generated from reading UI file 'authwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AUTHWINDOW_H
#define UI_AUTHWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AuthWindow
{
public:
    QWidget *centralwidget;
    QLabel *labelUsername;
    QLabel *labelPassword;
    QLineEdit *lineEditUsername;
    QLineEdit *lineEditPassword;
    QPushButton *pushButtonSign;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *AuthWindow)
    {
        if (AuthWindow->objectName().isEmpty())
            AuthWindow->setObjectName(QStringLiteral("AuthWindow"));
        AuthWindow->resize(314, 229);
        centralwidget = new QWidget(AuthWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        labelUsername = new QLabel(centralwidget);
        labelUsername->setObjectName(QStringLiteral("labelUsername"));
        labelUsername->setGeometry(QRect(40, 50, 91, 31));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        labelUsername->setFont(font);
        labelPassword = new QLabel(centralwidget);
        labelPassword->setObjectName(QStringLiteral("labelPassword"));
        labelPassword->setGeometry(QRect(40, 90, 91, 31));
        labelPassword->setFont(font);
        lineEditUsername = new QLineEdit(centralwidget);
        lineEditUsername->setObjectName(QStringLiteral("lineEditUsername"));
        lineEditUsername->setGeometry(QRect(140, 50, 113, 25));
        lineEditPassword = new QLineEdit(centralwidget);
        lineEditPassword->setObjectName(QStringLiteral("lineEditPassword"));
        lineEditPassword->setGeometry(QRect(140, 90, 113, 25));
        lineEditPassword->setEchoMode(QLineEdit::Password);
        pushButtonSign = new QPushButton(centralwidget);
        pushButtonSign->setObjectName(QStringLiteral("pushButtonSign"));
        pushButtonSign->setEnabled(false);
        pushButtonSign->setGeometry(QRect(170, 130, 89, 25));
        pushButtonSign->setFont(font);
        AuthWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(AuthWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 314, 22));
        AuthWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(AuthWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        AuthWindow->setStatusBar(statusbar);

        retranslateUi(AuthWindow);

        QMetaObject::connectSlotsByName(AuthWindow);
    } // setupUi

    void retranslateUi(QMainWindow *AuthWindow)
    {
        AuthWindow->setWindowTitle(QApplication::translate("AuthWindow", "MainWindow", Q_NULLPTR));
        labelUsername->setText(QApplication::translate("AuthWindow", "Username:", Q_NULLPTR));
        labelPassword->setText(QApplication::translate("AuthWindow", "Password:", Q_NULLPTR));
        lineEditPassword->setText(QString());
        pushButtonSign->setText(QApplication::translate("AuthWindow", "Sign in", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class AuthWindow: public Ui_AuthWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AUTHWINDOW_H
