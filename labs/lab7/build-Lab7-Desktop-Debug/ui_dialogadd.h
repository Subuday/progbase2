/********************************************************************************
** Form generated from reading UI file 'dialogadd.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGADD_H
#define UI_DIALOGADD_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>

QT_BEGIN_NAMESPACE

class Ui_DialogAdd
{
public:
    QDialogButtonBox *buttonBox;
    QLabel *labelMain;
    QLabel *labelName;
    QLabel *labelNation;
    QLabel *labelBorn;
    QLabel *labelDead;
    QLineEdit *lineEditNation;
    QLineEdit *lineEditBorn;
    QLineEdit *lineEditDead;
    QLineEdit *lineEditName;

    void setupUi(QDialog *DialogAdd)
    {
        if (DialogAdd->objectName().isEmpty())
            DialogAdd->setObjectName(QStringLiteral("DialogAdd"));
        DialogAdd->resize(400, 270);
        buttonBox = new QDialogButtonBox(DialogAdd);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(40, 230, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        labelMain = new QLabel(DialogAdd);
        labelMain->setObjectName(QStringLiteral("labelMain"));
        labelMain->setGeometry(QRect(60, 10, 271, 20));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        labelMain->setFont(font);
        labelName = new QLabel(DialogAdd);
        labelName->setObjectName(QStringLiteral("labelName"));
        labelName->setGeometry(QRect(20, 50, 71, 21));
        labelName->setFont(font);
        labelNation = new QLabel(DialogAdd);
        labelNation->setObjectName(QStringLiteral("labelNation"));
        labelNation->setGeometry(QRect(20, 90, 91, 21));
        labelNation->setFont(font);
        labelBorn = new QLabel(DialogAdd);
        labelBorn->setObjectName(QStringLiteral("labelBorn"));
        labelBorn->setGeometry(QRect(20, 130, 61, 21));
        labelBorn->setFont(font);
        labelDead = new QLabel(DialogAdd);
        labelDead->setObjectName(QStringLiteral("labelDead"));
        labelDead->setGeometry(QRect(20, 170, 67, 17));
        labelDead->setFont(font);
        lineEditNation = new QLineEdit(DialogAdd);
        lineEditNation->setObjectName(QStringLiteral("lineEditNation"));
        lineEditNation->setGeometry(QRect(130, 90, 191, 25));
        lineEditBorn = new QLineEdit(DialogAdd);
        lineEditBorn->setObjectName(QStringLiteral("lineEditBorn"));
        lineEditBorn->setGeometry(QRect(130, 130, 81, 25));
        lineEditDead = new QLineEdit(DialogAdd);
        lineEditDead->setObjectName(QStringLiteral("lineEditDead"));
        lineEditDead->setGeometry(QRect(130, 170, 81, 25));
        lineEditName = new QLineEdit(DialogAdd);
        lineEditName->setObjectName(QStringLiteral("lineEditName"));
        lineEditName->setGeometry(QRect(130, 50, 191, 25));

        retranslateUi(DialogAdd);
        QObject::connect(buttonBox, SIGNAL(accepted()), DialogAdd, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), DialogAdd, SLOT(reject()));

        QMetaObject::connectSlotsByName(DialogAdd);
    } // setupUi

    void retranslateUi(QDialog *DialogAdd)
    {
        DialogAdd->setWindowTitle(QApplication::translate("DialogAdd", "Dialog", Q_NULLPTR));
        labelMain->setText(QApplication::translate("DialogAdd", "Do you want to add a new writer?", Q_NULLPTR));
        labelName->setText(QApplication::translate("DialogAdd", "Name:", Q_NULLPTR));
        labelNation->setText(QApplication::translate("DialogAdd", "Nationality:", Q_NULLPTR));
        labelBorn->setText(QApplication::translate("DialogAdd", "Born:", Q_NULLPTR));
        labelDead->setText(QApplication::translate("DialogAdd", "Dead:", Q_NULLPTR));
        lineEditNation->setText(QApplication::translate("DialogAdd", "Ukrainian", Q_NULLPTR));
        lineEditBorn->setText(QApplication::translate("DialogAdd", "1814", Q_NULLPTR));
        lineEditDead->setText(QApplication::translate("DialogAdd", "1861", Q_NULLPTR));
        lineEditName->setText(QApplication::translate("DialogAdd", "Taras Shevchenko", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DialogAdd: public Ui_DialogAdd {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGADD_H
