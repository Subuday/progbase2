#ifndef DIALOGDELETE_H
#define DIALOGDELETE_H

#include <QDialog>
#include <QLabel>

namespace Ui {
class DialogDelete;
}

class DialogDelete : public QDialog
{
    Q_OBJECT

public:
    explicit DialogDelete(QWidget *parent = nullptr);
    ~DialogDelete();

    void setLabelWrID(QString id);
    void setLabelWrName(QString name);

private:
    Ui::DialogDelete *ui;

    QLabel* labelWrID;
    QLabel* labelWrName;

};

#endif // DIALOGDELETE_H
