#include "book.hpp"

bool Book::operator == (const Book& bk) const
{
    return this->Id == bk.getId();
}

void Book::operator = (const Book& bk)
{
    this->Id = bk.getId();
    this->Name = bk.getName();
    this->Genre = bk.getGenre();
    this->Year = bk.getYear();
}

void Book::setId(int id) {Id = id;}
void Book::setName(string name) {Name = name;}
void Book::setGenre(string genre) {Genre = genre;}
void Book::setYear(int year) {Year = year;}

int     Book::getId() const {return Id;}
string  Book::getName() const  {return Name;}
string  Book::getGenre() const  {return Genre;}
int     Book::getYear() const {return Year;}