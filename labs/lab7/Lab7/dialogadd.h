#ifndef DIALOGADD_H
#define DIALOGADD_H

#include <QDialog>
#include <QLineEdit>

#include "writer.hpp"

namespace Ui {
class DialogAdd;
}

class DialogAdd : public QDialog
{
    Q_OBJECT

public:
    explicit DialogAdd(QWidget *parent = nullptr);
    ~DialogAdd();
    Writer getWriter();

private slots:
    void updateWriter();

private:
    Ui::DialogAdd *ui;

    Writer writer;

    QValidator* validator;

    QLineEdit* lineEditName;
    QLineEdit* lineEditNation;
    QLineEdit* lineEditBorn;
    QLineEdit* lineEditDead;
};

#endif // DIALOGADD_H
