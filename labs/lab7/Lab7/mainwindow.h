#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidget>
#include <QPushButton>
#include <QLabel>

#include "dialogdelete.h"
#include "dialogedit.h"
#include "dialogadd.h"

#include "xml_storage.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_addButton_clicked();

    void on_editButton_clicked();

    void on_deleteButton_clicked();

    void onListWritersItemClicked(QListWidgetItem* item);

private:
    Ui::MainWindow *ui;
    QListWidget *listWidget;

    QPushButton *addButton;
    QPushButton *editButton;
    QPushButton *deleteButton;

    QLabel *labelSelectObj;
    QLabel *labelID;
    QLabel *labelName;
    QLabel *labelNation;
    QLabel *labelBorn;
    QLabel *labelDead;
    QLabel *labelWriterID;
    QLabel *labelWriterName;
    QLabel *labelWriterNation;
    QLabel *labelWriterBorn;
    QLabel *labelWriterDead;

    XmlStorage* storage;

    void loadDataToListWidget();
    void labelsSetEnabledState(bool state);

};

#endif // MAINWINDOW_H
