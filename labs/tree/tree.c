#include "tree.h"

struct BinTree 
{
   char* value;      // set on init/alloc
   BinTree* left;  // set to NULL on init
   BinTree* right; // set to NULL on init
};

static void BinTree_free (BinTree * self);
static BinTree* lookupPlaceForKey(BinTree * self, int value);
static BinTree* BinTree_alloc();
static void BinTree_init (BinTree* self, StrStrMap* value);

static BinTree* BinTree_alloc () {
    BinTree* self = (BinTree*)malloc(sizeof(struct BinTree));
    if(self == NULL) {fprintf(stderr, "BinTree_allo: BinTree* root malloc NULL"); abort();}
    self->value = NULL;
    self->right = NULL;
    self->left = NULL;

    return self;
}

static void BinTree_init (BinTree* self, char* value) {
    self->value = value;
}

BSTree* BSTree_alloc() {
    BSTree* self = (BSTree*)malloc(sizeof(struct BSTree));
    if(self == NULL) {fprintf(stderr, "BSTree_alloc: BSTree* self malloc NULL"); abort();}

    BinTree* root = NULL;

    self->root = root;
    self->size = 0;

    return self;
} 

static void BinTree_free (BinTree * self) {
    if(self->right != NULL) BinTree_free(self->right);
        else if(self->left != NULL) BinTree_free(self->left);

    free(self);
}

void BSTree_free(BSTree* self) {
    BinTree_free(self->root);
    free(self);
}

static BinTree* lookupPlaceForKey(BinTree* self, int value) {
    if(self == NULL) {self = BinTree_alloc(); return self;}

    char* idValue = StrStrMap_get(self->value, "id");
    int id = atoi(idValue);

    if(self != NULL && id > value) self = lookupPlaceForKey(self->right, value);
    else if(self != NULL && id <= value) self = lookupPlaceForKey(self->left, value);

    self = BinTree_alloc();

    return self;
}

void BSTree_insert (BSTree * self, char* value) {
    char* idValue = StrStrMap_get(value, "id");
    int id = atoi(idValue);

    BinTree* node = lookupPlaceForKey(self->root, id);
    BinTree_init(node, value);
}

