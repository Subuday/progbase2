#pragma once

#include <stdlib.h>
#include <stdio.h> 
#include <string.h>
#include <stdbool.h>

typedef struct BSTree BSTree;
typedef struct BinTree BinTree;

struct BSTree {
    BinTree * root;  // a pointer to the root tree node
    size_t size;     // store number of added items to make _size() O(1)
};

BSTree* BSTree_alloc();
void BSTree_free(BSTree* self);
void BSTree_insert (BSTree * self, char* value);
void BSTree_postorderPrint(BinTree * self);






